<%-- 
    Document   : adminLogin
    Created on : Feb 25, 2016, 6:33:19 PM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>
<%@ page session="false" %>
<%@page import="az.edu.ada.carental.enums.ErrorTypes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Admin</title>
        <!-- BOOTSTRAP STYLES-->
        <link href="${pageContext.request.contextPath}/adminPanel/assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLES-->
        <link href="${pageContext.request.contextPath}/adminPanel/assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
        <link href="${pageContext.request.contextPath}/adminPanel/assets/css/custom.css" rel="stylesheet" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    </head>
    <body>
        <div class="container">
            <div class="row text-center ">
                <div class="col-md-12">
                    <br /><br />
                    <h2> Admin Panel Login</h2>
                    <h5>( Only Administrator must login )</h5>
                    <br />
                </div>
            </div>
            <div class="row ">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>Enter Details To Login </strong>  
                        </div>
                        <div class="panel-body">
                            <form role="form" action="${pageContext.request.contextPath}/aLogin" method="post" >
                                <br />
                                <c:choose>
                                    <c:when test="${requestScope.error == 'WRONG_CREDENTIALS'}">
                                        <small style="color: red">Name or password is wrong</small>
                                    </c:when>
                                    <c:when test="${requestScope.error == 'BLANK_CREDENTIALS'}">
                                        <small style="color: red">Fields must be filled</small>
                                    </c:when>
                                     <c:when test="${requestScope.error == 'ERROR_IN_WEB_XML'}">
                                        <small style="color: red">Something wrong with configuration</small>
                                    </c:when>
                                </c:choose>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                    <input type="text" name="name" class="form-control" value="${requestScope.name}" placeholder="Name" />
                                </div> 
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                        <input type="password" name="password" class="form-control"  placeholder="Password" />
                                </div>
                                <button class="btn btn-primary ">Login</button>
                                <hr />
                            </form>
                        </div>

                    </div>
                </div>


            </div>
        </div>
        <!-- JQUERY SCRIPTS -->
        <script src="${pageContext.request.contextPath}/adminPanel/assets/js/jquery-1.10.2.js"></script>
        <!-- BOOTSTRAP SCRIPTS -->
        <script src="${pageContext.request.contextPath}/adminPanel/assets/js/bootstrap.min.js"></script>
    </body>
</html>
