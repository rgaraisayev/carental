<%-- 
    Document   : tables
    Created on : Mar 8, 2016, 12:09:48 AM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/adminPanel/assets/css/custom_tables.css"/>
<c:choose>
    <c:when test="${requestScope.table eq 'users'}">
        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Users
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <c:forEach items="${requestScope.userTableC}" var="c" >
                                            <th>${c}</th>
                                            </c:forEach>
                                        <th>Delete</th>
                                        <th>Edit</th>
                                        <th>Ban</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${requestScope.users}" var="user">
                                        <tr>                                        
                                            <td>${user.id}</td>
                                            <td>${user.name}</td>
                                            <td>${user.surname}</td>
                                            <td>${user.email}</td>
                                            <td> </td>
                                            <td>${user.phone}</td>
                                            <td>${user.profileImageUrl}</td>
                                            <td>${user.type}</td>
                                            <td>Delete</td>
                                            <td>Edit</td>
                                            <td>Ban</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <c:forEach items="${requestScope.userTableC}" var="c" >
                                            <th>${c}</th>
                                            </c:forEach>
                                        <th>Delete</th>
                                        <th>Edit</th>
                                        <th>Ban</th>
                                    </tr>
                                </tfoot> 
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>

    </c:when>
    <c:when test="${requestScope.table eq 'cars'}">
        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Cars
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-1">
                                <thead>
                                    <tr>
                                        <c:forEach items="${requestScope.carTableC}" var="c" >
                                            <th>${c}</th>
                                            </c:forEach>
                                        <th>Delete</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${requestScope.cars}" var="car">
                                        <tr>                                        
                                            <td>${car.id}</td>
                                            <td>${car.model.id}</td>
                                            <td>${car.color}</td>
                                            <td>${car.description}</td>
                                            <td>${car.imageUrl}</td>
                                            <td>${car.horsePower}</td>
                                            <td>${car.status}</td>
                                            <td>${car.year}</td>
                                            <td>${car.engineVolume}</td>
                                            <td>${car.fuelType}</td>
                                            <td>${car.transmissionType}</td>
                                            <td>${car.differentialType}</td>
                                            <td>${car.userId}</td>
                                            <td>${car.price}</td>
                                            <td>${car.rentType}</td>
                                            <td>${car.currency}</td>
                                            <td>${car.uuid}</td>
                                            <td>Delete</td>
                                            <td>Edit</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <c:forEach items="${requestScope.carTableC}" var="c" >
                                            <th>${c}</th>
                                            </c:forEach>
                                        <th>Delete</th>
                                        <th>Edit</th>
                                    </tr>
                                </tfoot>
 
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </c:when>

    <c:when test="${requestScope.table eq 'requests'}">
        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Requests
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTable_requests">
                                <thead>
                                    <tr>
                                        <c:forEach items="${requestScope.requestTableC}" var="c" >
                                            <th>${c}</th>
                                            </c:forEach>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${requestScope.requests}" var="request">
                                        <tr>                                        
                                            <td>${request.id}</td>
                                            <td>${request.user.id}</td>
                                            <td>${request.car.uuid}</td>
                                            <td>${request.status}</td>
                                            <td>${request.date}</td>
                                            <td>Delete</td>
                                            <td>Edit</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <c:forEach items="${requestScope.requestTableC}" var="c" >
                                            <th>${c}</th>
                                            </c:forEach>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <nav>
                            <ul class="pagination" id="pages">

                            </ul>
                        </nav>
                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </c:when>
</c:choose>
