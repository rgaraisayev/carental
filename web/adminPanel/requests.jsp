<%-- 
    Document   : requests
    Created on : Mar 8, 2016, 2:27:28 AM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>

<%@page import="az.edu.ada.carental.entities.Request"%>
<%@page import="az.edu.ada.carental.enums.RequestStatus"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/jquery.bxslider.css"/>
<script src="${pageContext.request.contextPath}/user/js/jquery.bxslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/jquery.bxslider.min.js"></script>
<div class="row">
    <!--column one-->
    <c:forEach items="${requestScope.requests}" var="requestItem">
        <div class="col-md-6 col-sm-12 col-xs-12">           
            <div class="panel panel-back noti-box">
                <% pageContext.setAttribute("btnAcceptStatus", (RequestStatus) ((Request) pageContext.getAttribute("requestItem")).getStatus() != RequestStatus.ACCEPTED || pageContext.getAttribute("requestItem") == RequestStatus.PENDING ? "" : "disabled"); %>
                <% pageContext.setAttribute("btnRejectStatus", (RequestStatus) ((Request) pageContext.getAttribute("requestItem")).getStatus() != RequestStatus.REJECTED || pageContext.getAttribute("requestItem") == RequestStatus.PENDING ? "" : "disabled");%>
                <button style="float:  right" ${btnRejectStatus} type="button" onclick="reject(${requestItem.id}, ${requestItem.car.id})" id="reject-${requestItem.id}" class="btn btn-danger btn-circle"><i class="fa fa-warning"></i>
                </button>
                <button style="float:  right" type="button" ${btnAcceptStatus} onclick="accept(${requestItem.id}, ${requestItem.car.id})" id="accept-${requestItem.id}" class="btn btn-success btn-circle"><i class="fa fa-check"></i>
                </button>
                <div class="text-box" >              
                    <p  class="main-text">Name: ${requestItem.user.name}</p>
                    <p  class="main-text">Surname:  ${requestItem.user.surname} </p>
                    <p class="text-muted">Email: ${requestItem.user.email} </p>
                    <p class="text-muted">Phone: ${requestItem.user.phone} </p>
                    <p class="text-muted">Status: 
                        <c:choose>
                            <c:when test="${requestItem.status == 'ACCEPTED'}"><label id="label-${requestItem.id}" class="label label-success">ACCEPTED</label> </c:when>
                            <c:when test="${requestItem.status == 'REJECTED'}"><label id="label-${requestItem.id}" class="label label-danger">REJECTED</label> </c:when>
                            <c:when test="${requestItem.status == 'PENDING'}"><label id="label-${requestItem.id}" class="label label-warning">PENDING</label> </c:when>
                        </c:choose>
                    </p>
                    <p class="text-muted">Request Date: ${requestItem.date}</p>
                    <hr />
                    <p class="text-muted">
                        <span class="text-muted color-bottom-txt"><i class="fa fa-angle-down"></i>
                            <label>Description:</label> ${requestItem.car.description}
                        </span>
                    </p>
                    <hr/>
                    <table>
                        <tr>
                        <label>Brand:</label> ${requestItem.car.model.brand.name}<br/>
                        </tr>
                        <tr>
                        <label>Model:</label> ${requestItem.car.model.name}<br/>
                        </tr>
                        <tr>
                        <label>Year:</label> ${requestItem.car.year}<br/>
                        </tr>
                        <tr>
                        <label>Horse power:</label> ${requestItem.car.horsePower}<br/>
                        </tr>
                        <tr>
                        <label>Color:</label> ${requestItem.car.color}<br/>
                        </tr>
                        <tr>
                        <label>Transmission:</label> ${requestItem.car.transmissionType}<br/>
                        </tr>
                        <tr>
                        <label>Differential:</label> ${requestItem.car.differentialType}<br/>
                        </tr>
                        <tr>
                        <label>Engine Volume:</label> ${requestItem.car.engineVolume} sm3<br/>
                        </tr>
                        <tr>
                        <label>Fuel Type:</label> ${requestItem.car.fuelType}<br/>
                        </tr>
                        <tr>
                        <label>Year:</label> ${requestItem.car.year}<br/>
                        </tr>
                        <tr>
                        <label>Horse Power:</label> ${requestItem.car.horsePower}<br/>
                        </tr>
                        <tr>
                        <label>Rent Type:</label> ${requestItem.car.rentType}<br/>
                        </tr>
                        <tr>
                        <label>Price:</label> ${requestItem.car.price} ${requestItem.car.currency}<br/>
                        </tr>
                        <tr>
                        <label>Status:</label> ${requestItem.car.status}
                        </tr>
                    </table>

                    <p class="text-muted">
                        <span class="text-muted color-bottom-txt"><i class="fa fa-angle-down"></i>
                            <!--<label>Image:</label><img  class="col-md-6 col-sm-12 col-lg-12" src="${requestItem.car.imageUrl}"/>-->

                            <ul class="bxslider">
                                <c:forEach items="${requestItem.car.imageUrls}" var="url">
                                    <li><img  class="col-md-6 col-sm-12 col-lg-12" src="${url}" title="Funky roots" /></li>
                                    </c:forEach>
                            </ul>
                        </span>
                    </p>

                    <p class="text-muted"></p><p class="text-muted"></p><p class="text-muted"></p><p class="text-muted"></p><p class="text-muted">.</p>
                </div>
            </div>
        </div>
    </c:forEach>
</div>

<script>

    $(document).ready(function () {
        $('.bxslider').bxSlider({
            mode: 'fade',
            auto: true,
            autoControls: true
        });
    });
    function accept(id, car_id) {
        $.post("request", {id: id, car_id: car_id,  attemp: "accept"}).done(function () {
            $("#accept-" + id).prop("disabled", true);
            $("#reject-" + id).prop("disabled", false);
            $("#label-" + id).attr('class', 'label label-success').text("ACCEPTED");
        }).fail(function () {
            alert("error");
        });
    }

    function reject(id, car_id) {
        $.post("request", {id: id, car_id: car_id,  attemp: "reject"}).done(function () {
            $("#reject-" + id).prop("disabled", true);
            $("#accept-" + id).prop("disabled", false);
            $("#label-" + id).attr('class', 'label label-danger').text("REJECTED");
        }).fail(function () {
            alert("error");
        });
    }


</script>
