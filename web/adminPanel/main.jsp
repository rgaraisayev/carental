<%-- 
    Document   : main
    Created on : Mar 8, 2016, 12:19:27 AM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>
<div class="row">
    <a href="request?status=accepted"><div class="col-md-4 col-sm-6 col-xs-6">           
            <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-check-circle"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">${requestScope.acceptedRequestCount} Request</p>
                    <p class="text-muted">Accepted</p>
                </div>
            </div>
        </div></a>
    <a href="request?status=rejected"><div class="col-md-4 col-sm-6 col-xs-6">           
            <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-warning"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">${requestScope.rejectedRequestCount} Request</p>
                    <p class="text-muted">Rejected</p>
                </div>
            </div>
        </div></a>
    <a href="request?status=pending"><div class="col-md-4 col-sm-6 col-xs-6">           
            <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                    <i class="fa fa-clock-o"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">${requestScope.pendingRequestCount} Request</p>
                    <p class="text-muted">Pending</p>
                </div>
            </div>
        </div></a>
</div>
<!-- /. ROW  -->
<hr />             
<div class="row">
    <div class="col-md-12">
        <h4>Statistics </h4>
    </div>
</div>
<!-- /. ROW  -->
<div class="row">
    <div class="col-md-4 col-sm-12 col-xs-12">                       
        <div class="panel panel-primary text-center no-boder bg-color-green">
            <div class="panel-body">
                <i class="fa fa-bar-chart-o fa-5x"></i>
                <h3>${requestScope.RPU}</h3>
                <h3>request/user  </h3>
            </div>
            <div class="panel-footer back-footer-green">
                RPU
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">   
        <div class="panel panel-primary text-center no-boder bg-color-red">
            <div class="panel-body">
                <i class="fa fa-edit fa-5x"></i>
                <h3>2 users/day </h3>
                <h3>2 quests/day </h3>
            </div>
            <div class="panel-footer back-footer-red">
                Traffic
            </div>
        </div>                         
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">   
        <div class="panel panel-primary text-center no-boder bg-color-red">
            <div class="panel-body">
                <i class="fa fa-edit fa-5x"></i>
                <h3> Online: ${requestScope.onlineUsers}</h3>
                <h3>Overall: ${requestScope.guests}</h3>
            </div>
            <div class="panel-footer back-footer-red">
                Users    

            </div>
        </div>                         
    </div>

</div>

<hr/>
