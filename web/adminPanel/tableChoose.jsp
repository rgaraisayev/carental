<%-- 
    Document   : tableChoose
    Created on : Apr 29, 2016, 5:37:31 PM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/adminPanel/assets/css/custom_tables.css"/>
<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
           <a href="${pageContext.request.contextPath}/tables?tables=users"><div class="panel-heading">
                Users
                </div></a>

        </div>
        <!--End Advanced Tables -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
            <a href="${pageContext.request.contextPath}/tables?tables=cars"><div class="panel-heading">
                Cars
                </div></a>

        </div>
        <!--End Advanced Tables -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
            <a href="${pageContext.request.contextPath}/tables?tables=requests"><div class="panel-heading">
                Requests
                </div></a>

            
        </div>
        <!--End Advanced Tables -->
    </div>
</div>