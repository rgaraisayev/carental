<%-- 
    Document   : index.jsp
    Created on : Mar 7, 2016, 11:48:53 PM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Admin Panel</title>
        <!-- BOOTSTRAP STYLES-->
        <link href="${pageContext.request.contextPath}/adminPanel/assets/css/bootstrap.css" rel="stylesheet" />
        <link href="http://datatables.net/release-datatables/extensions/ColVis/css/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
        <!-- FONTAWESOME STYLES-->
        <link href="${pageContext.request.contextPath}/adminPanel/assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
        <link href="${pageContext.request.contextPath}/adminPanel/assets/css/custom.css" rel="stylesheet" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">Binary admin</a> 
                </div>
                <div style="color: white;
                     padding: 15px 50px 5px 50px;
                     float: right;
                     font-size: 16px;"> Last access : ${sessionScope.lastAccessedTime} &nbsp; <a href="${pageContext.request.contextPath}/aLogin?action=logout" class="btn btn-danger square-btn-adjust">Logout</a> </div>
            </nav>   
            <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">
                        <li class="text-center">
                            <img src="https://img01.olx.co.ke/images_olxke/1000583808_1_644x461_systems-software-developer-programmer-database-servers-admin-nairobi-cbd.jpg" class="user-image img-responsive"/>
                        </li>
                        <li>
                            <a   href="${pageContext.request.contextPath}/main"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                        </li>
                        <li>
                            <a  href="${pageContext.request.contextPath}/tables?tables=all"><i class="fa fa-desktop fa-3x"></i> Tables </a>
                        </li>
                    </ul>

                </div>

            </nav>  
            <!-- /. NAV SIDE  -->
            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Admin Dashboard</h2>   
                            <h4>Welcome ${sessionScope.name}, Love to see you back. </h4>
                        </div>
                    </div>
                    <!-- /. ROW  -->
                    <hr />

                    <c:choose>
                        <c:when test="${requestScope.navigation == 'TABLE_CHOOSE'}"><jsp:include page="tableChoose.jsp"></jsp:include></c:when>
                        <c:when test="${requestScope.navigation == 'TABLES'}"><jsp:include page="tables.jsp"></jsp:include></c:when>
                        <c:when test="${requestScope.navigation == 'STATUS_VIEW'}"><jsp:include page="requests.jsp"></jsp:include></c:when>
                        <c:otherwise><jsp:include page="main.jsp"></jsp:include></c:otherwise>
                    </c:choose>

                    <!-- /. ROW  -->           
                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE WRAPPER  -->
        </div>
        <!-- /. WRAPPER  -->
        <!-- METISMENU SCRIPTS -->
    </body>
</html>

