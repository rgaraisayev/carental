<%-- 
    Document   : login
    Created on : Feb 21, 2016, 8:12:44 PM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<div class="single_page_content fix">
    <h1 style="margin-bottom:15px;">Rent Your Car</h1><br>

    <div class="contact_form">
        <form action="${pageContext.request.contextPath}/UploadImageServlet" method="post" enctype="multipart/form-data">
            <c:choose>
                <c:when test="${requestScope.brand}">
                    <center><label style=" padding-bottom: 5px; color:red">Choose Brand</label></center>
                    </c:when>
                    <c:when test="${requestScope.model}">
                    <center><label style=" padding-bottom: 5px; color:red">Choose Model</label></center>
                    </c:when>
                    <c:when test="${requestScope.year}">
                    <center><label style=" padding-bottom: 5px; color:red">Wring Year</label></center>
                    </c:when>
                    <c:when test="${requestScope.engineVolume}">
                    <center><label style=" padding-bottom: 5px; color:red">Wrong Engine Volume</label></center>
                    </c:when>
                    <c:when test="${requestScope.color}">
                    <center><label style=" padding-bottom: 5px; color:red">Wrong Color</label></center>
                    </c:when>
                    <c:when test="${requestScope.price}">
                    <center><label style=" padding-bottom: 5px; color:red">Wrong Price</label></center>
                    </c:when>
                </c:choose>
            <center><p>Choose Brand
                    <select id="brand" class="text" name="brand">
                        <c:forEach items="${requestScope.brands}" var="brand">
                            <option value="${brand.name}">${brand.name}</option>
                        </c:forEach>
                    </select>
                </p></center><br><br>
            <center><p>Choose Model
                    <select id="model" class="text" name="model">
                    </select>
                </p></center><br><br>

            <center><p><textarea id="desc"type="text" class="text" name="description" value="${requestScope.description}" placeholder="Description"></textarea></p></center><br><br>
            <center><p><input type="number" class="text" name="price" value="${requestScope.price}" placeholder="Price"/>
                    <select style="margin-right: -100px; padding: 5px" name="currency">
                        <c:forEach items="${requestScope.currency}" var="cur">
                            <option value="${cur}">${cur}</option>
                        </c:forEach>
                    </select>
                </p></center>
            <center><h3 style="margin-top: 40px; padding: 5px">Spesifications</h3></center><br><br>
            <center><p><input type="text" class="text" name="color" placeholder="Color"/></p></center><br><br>
            <center><p><input type="number" class="text" name="horsePower" placeholder="Horse Power"/></p></center><br><br>
            <center><p><input type="number" class="text" name="year" placeholder="Year"/></p></center><br><br>
            <center><p><input type="text" class="text" name="engineVolume" placeholder="Engine Volume"/>cm3</p></center><br><br>
            <center><p>Choose Fuel <br/>
                   <select id="fuelType" class="text" name="fuelType">
                            <option value="GASOLINE">GASOLINE</option>
                    </select>
                </p></center><br><br>
                  <center><p>Choose Transmission<br/>
                   <select id="transType" class="text" name="transType">
                            <option value="AVTOMAT">AVTOMAT</option>
                            <option value="MANUAL">MANUAL</option>
                    </select>
                </p></center><br><br>
                <center><p>Choose Differential<br/>
                   <select id="diffType" class="text" name="diffType">
                            <option value="FRONT">FRONT</option>
                            <option value="BACK">BACK</option>
                            <option value="BOTH">BOTH</option>
                    </select>
                </p></center><br><br>
                <center><p>Choose Rent type<br/>
                   <select id="rentType" class="text" name="rentType">
                            <option value="DAILY">DAILY</option>
                            <option value="WEEKLY">WEEKLY</option>
                            <option value="MONTHLY">MONTHLY</option>
                            <option value="YEARLY">YEARLY</option>
                    </select>
                </p></center><br><br>
            <center><h3>Images</h3></center><br><br>
            <center>Main Image
            <input type="file" name="mainImage" accept="image/x-png, image/gif, image/jpeg"  /></center>
            <center  class="opt" >Optional Images</center><center><input type="file" class="image" name="image" accept="image/x-png, image/gif, image/jpeg"  /></center>
                <center>  <input class="image" type="file" name="image1" accept="image/x-png, image/gif, image/jpeg"  /></center>
                <center>    <input class="image" type="file" name="image2" accept="image/x-png, image/gif, image/jpeg"  /></center>
                <center>    <input class="image" type="file" name="image3" accept="image/x-png, image/gif, image/jpeg"  /></center>
            <center><p><input type="submit" class="submit" id="sbtn" value="Publish"/></p></center><br>
       </form>
    </div>


</div>
<script>
    $(document).on("ready", function () {
        getModelsForRent();
    });


    $("#brand").change(function () {
        getModelsForRent();
    });

    function getModelsForRent() {
        $("#model").html("");
        $.post("RentYourCar", {name: $("#brand option:selected").text()}, function (data) {
            var models = JSON.parse(data);
            var options = "";
            $.each(models, function (index, model) {
                options += "<option value=" + model.id + ">" + model.name + "</option>";
            });
            $("#model").html("");
            $("#model").append(options);
        });
    }



</script>
<style>
    .image{
        margin-top: 10px;
        margin-left: 40px;
    }

    .opt{
        margin-top: 10px;
        margin-left: -40px;
    }

    #desc{
        margin-left: 90px;
    }

    #sbtn{
        margin-top: 30px;
    }
</style>