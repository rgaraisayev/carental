<%-- 
    Document   : login
    Created on : Feb 21, 2016, 8:12:44 PM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<div class="single_page_content fix">
    <h1 style="margin-bottom:15px;">Sign In</h1><br>

    <div class="contact_form">
        <form action="${pageContext.request.contextPath}/login" method="post">
            <c:if test="${requestScope.notRegistered}">
                <center><label style=" padding-bottom: 5px; color:red">Wrong email and password</label></center>
                </c:if>
                <c:if test="${requestScope.wrong}">
                <center><label style=" padding-bottom: 5px; color:red">Please fill the blanks</label></center>
                </c:if>
            <center><p><input type="text" class="email" name="email" value="${requestScope.email}" placeholder="Email"/></p></center><br><br>
            <center><p><input type="password" class="text" name="password" placeholder="Password"/></p></center><br><br>
            <center><p><input type="submit" class="submit" value="Sign In"/></p></center><br>
            <center><p style="font-family:san-serif,arial; font-size:14px;line-height:20px; color: grey"><i>OR</i></p></center><br>
            <center><a href="${pageContext.request.contextPath}/index?page=register"><p style="font-family:san-serif,arial;font-size:30px;line-height:20px; color: grey">Register Now</p></a></center>
        </form>
    </div>


</div>
