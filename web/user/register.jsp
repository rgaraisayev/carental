<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script src="${pageContext.request.contextPath}/user/js/intlTelInput.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/intlTelInput.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/help-tips.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/jss/validation.js"/>

<div class="single_page_content fix">
    <h1 style="margin-bottom:15px;">Register</h1><br>

    <div class="contact_form">
        <form action="register" method="post" >
            <c:if test="${requestScope.blank}">
                <center><label id="blanks" style=" padding-bottom: 5px; color:red">Fill all the blanks</label></center>
                </c:if><c:if test="${requestScope.error}">
                <center><label style=" padding-bottom: 5px; color:red">Error code: 500</label></center>
                </c:if>
            <center><label id="blanks" style=" padding-bottom: 5px; color:red"></label></center>
            <center><p><span style="color: red; margin-right: 10px">*</span><input type="text" class="text" id="name" name="name" value="${name}" placeholder="Name"/></p></center><br><br>
            <center><p><span style="color: red; margin-right: 10px">*</span><input type="text" class="text" id="surname"  name="surname" value="${surname}" placeholder="Surname"/></p></center><br><br>        
                        <c:if test="${requestScope.emailExists}">
                <center><label  style=" padding-bottom: 5px; color:red">This email is used</label></center>
                </c:if>
            <center><label  id="emailError" style=" padding-bottom: 5px; color:red"></label></center>
            <center><p><span style="color: red; margin-right: 10px">*</span><input type="text" class="email" name="email" id="email"  value="${email}" oninput="validateEmail()" placeholder="Email"/></p></center><br><br>
            <h5 style="margin-bottom: 50px; margin-left: 125px"><span style="color: red; margin-right: 10px">*</span><input  class="text"  type="tel" name="phone" value="${phone}"  id="phone" onkeydown="return checkNumber(event)" placeholder="Phone number"/></h5>
            <!--            <div class="help-tip">
                            <p>This is the inline help tip! You can explain to your users what this section of your web app is about.</p>
                        </div>-->
            <center><label  id="strongPass"></label></center>
            <center><p><span style="color: red; margin-right: 10px">*</span><input type="password" id="password" class="text" oninput="passwordStrongnessChecker()" name="password" placeholder="Password"/></p></center><br><br>

            <center><label  id="equalPass"></label></center>
            <center><p><span style="color: red; margin-right: 10px">*</span><input type="password" id="password2" oninput="passwordEqualityChecker()" class="text" name="password2" placeholder="Repeat the Password"/></p></center><br><br>
                        <c:if test="${requestScope.passDontMatch}">
                <center><label style=" padding-bottom: 5px; color:red">Passwords do not match</label></center>
                </c:if>
            <center><p><input type="submit" class="submit" value="Register Now"/></p></center>
        </form>
    </div>
</div>
<script runat="server">
    $(document).ready(function () {
        $("#phone").intlTelInput({
            onlyCountries: ["az"]
        });
    });
    $("form").submit(function () {
        if (checkBlanks() && validateEmail() && passwordStrongnessChecker() && passwordEqualityChecker()) {
            $("#phone").val($("#phone").intlTelInput("getNumber"));
        } else
            return false;
    });




    function checkNumber(event) {
        return event.which >= 48 && event.which <= 57 || event.which == 8 || event.which == 46 || event.which == 37 || event.which == 39;
    }

    function validateEmail() {
        var email = $("#email").val();
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email)) {
            $("#emailError").html("Not valid email");
        } else {
            $("#emailError").html("");
        }
        return re.test(email);
    }

    function passwordStrongnessChecker() {
        var pass = $("#password").val();
        var array = ["&", "*", "$", "@", "!", "#", "%", "^", "(", "{", "}", ")", "`", "~", "/", ".", ",", ":", "?", "+", "-", ";"];
        var containsCount = 0;
        for (var i = 0; i < array.length; i++) {
            if (pass.indexOf(array[i]) > -1) {
                containsCount++;
            }
        }
        if (pass.match(/\d+/g) != null) {
            containsCount++;
        }
        if (pass.length >= 6 && containsCount >= 3) {
            //  very strong
            $("#strongPass").css({"color": "green"});
            $("#strongPass").text("Very Strong");

        } else if (pass.length > 4 && containsCount >= 2) {
            // strong
            $("#strongPass").css({"color": "grey"});
            $("#strongPass").text("Strong");
        } else if ((pass.length > 4 && containsCount > 0) || pass.length > 8) {
            // good
            $("#strongPass").css({"color": "blue"});
            $("#strongPass").text("Good");
        } else {
            // bad
            $("#strongPass").css({"color": "red"});
            $("#strongPass").text("Bad password");
            return false;
        }
        return true;
    }

    function passwordEqualityChecker() {
        if ($("#password").val() != $("#password2").val()) {
            $("#equalPass").css("color", "red");
            $("#equalPass").text("Passwords are not equal");
            return false;
        } else {
            $("#equalPass").text("");
        }
        return true;
    }

    function checkBlanks() {
        if ($("#name").val() != "" && $("#surname").val() != "" && $("#phone").val() != "") {
            $("#blanks").html("");
            return true;
        } else {
            $("#blanks").html("Fill all the blanks");
        }
    }

</script>