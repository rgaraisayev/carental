
<%@page import="az.edu.ada.carental.entities.Car"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/jquery.bxslider.css"/>
<script src="${pageContext.request.contextPath}/user/js/jquery.bxslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/jquery.bxslider.min.js"></script>
<div class="single_page_content fix">
    <div class="fix single_content_info">       
        <ul class="bxslider">
            <c:forEach items="${car.imageUrls}" var="url">
                <li><img src="${url}" title="Funky roots" /></li>
                </c:forEach>
        </ul>
        <c:choose>
            <c:when test="${car.user.id == sessionScope.loggedUser.id}">
                <h3 id="yourcar" style="color:#DB0630; font-size: 15px; margin-bottom: 20px">This is your car</h3>
                <c:if test="${rentee ne 'NULL' && rentee.id > 0}">
                    <div id="renterInformation">
                        <h3 style="color:#DB0630; font-size: 13px; margin-bottom: 5px">It is being rented by</h3>
                        <p style="color:#000">Name: ${rentee.name} ${rentee.surname}</p>
                        <p style="color:#000">Phone: ${rentee.phone}</p>
                        <p style="color:#000">Email: ${rentee.email}</p>
                        <button id="finishProcess" style="margin-top: 10px; padding: 4px; margin-bottom: 15px">Finish Rent</button> <label style="color:#000">* by clicking you make it available for all users</label>
                    </div>
                </c:if>
            </c:when>
            <c:when test="${!sessionScope.isLogged && car.status eq 'AVAILABLE'}">
                <center><h3 style="color:#DB0630; font-size: 18px; margin-bottom: 40px">Log in to rent this car</h3></center>
                </c:when>
                <c:when test="${car.status eq 'AVAILABLE'}">
                <center><h3 id="rentnow" style="color:#DB0630; font-size: 18px; margin-bottom: 40px">Rent Now !</h3></center>
                <center><p id="waiting" style="display: none; color:#1E2021; font-size: 13px; margin-bottom: 20px">(Waiting for renter to accept)</p></center>
                </c:when>
                <c:when test="${rent ne 'NULL' && rent.renteeId > 0 && rent.renteeId == sessionScope.loggedUser.id}">
                    <c:choose >
                        <c:when test="${car.status eq 'AT_RENT' &&  rent.status eq 'CURRENT'}">
                        <center><h3 id="rentAccepted" style="color:green; font-size: 18px; margin-bottom: 40px">Your request accepted</h3></center>
                        <center><p id="waiting" style="color:#1E2021; font-size: 13px; margin-bottom: 20px">(This message will go off when rent is over)</p></center>
                        </c:when>
                        <c:when test="${rent.status eq 'CANCELED'}">
                        <center><h3 id="rentAccepted" style="color:red; font-size: 18px; margin-bottom: 40px">Your request canceled</h3></center>
                        <center><p id="waiting" style="color:#1E2021; font-size: 13px; margin-bottom: 20px">(You have to wait for 5 min to request again)</p></center>
                        </c:when>
                        <c:when test="${car.status eq 'AT_RENT'  && rent.status eq 'REQUESTED_FOR_RENT'}">
                        <center><h3 id="rentnow" style="color:#DB0630; font-size: 18px; margin-bottom: 40px">Pending..</h3></center>
                        <center><p id="waiting" style="color:#1E2021; font-size: 13px; margin-bottom: 20px">(Waiting for renter to accept)</p></center>
                        </c:when>
                    </c:choose>
                </c:when>
            </c:choose>
        <div id="rentingDetails" style="display: none; padding: 0px 0px 40px 3px">
            <h4 style="color:#000">Renter Information</h4>
            <p style="color:#000">Name: ${car.user.name} ${car.user.surname}</p>
            <p style="color:#000">Phone: ${car.user.phone}</p>
            <p style="color:#000">Email: ${car.user.email}</p>
            <c:choose>
                <c:when test="${rent ne 'NULL' && rent.status eq 'REQUESTED_FOR_RENT' and rent.renteeId == sessionScope.loggedUser.id}">
                    <button id="rentrequest" style="margin-top: 10px; padding: 4px">Cancel Request</button>
                </c:when>
                <c:otherwise>
                    <button id="rentrequest" style="margin-top: 10px; padding: 4px">Request for rent</button>
                </c:otherwise>
            </c:choose>
        </div>

        <div id="rentInformation" style="display: none; padding: 0px 0px 40px 3px">
            <h4 style="color:#000">Renter Information</h4>
            <p style="color:#000">Name: ${car.user.name} ${car.user.surname}</p>
            <p style="color:#000">Phone: ${car.user.phone}</p>
            <p style="color:#000">Email: ${car.user.email}</p>
        </div>
        <hr/>
        <h3 style="color:#000">Description</h3>
        <h3 style="color:#888">${car.description}</h3>
        <table  border="1" style=" font-size: 16px; width: 100%;border-collapse: collapse;" cellspacing="10" cellpadding="6">
            <tbody>
                <tr>
                    <td>Brand</td>
                    <td>${car.model.brand.name}</td>
                </tr>
                <tr>
                    <td>Model</td>
                    <td>${car.model.name}</td>
                </tr>
                <tr>
                    <td>Color</td>
                    <td>${car.color}</td>
                </tr>
                <tr>
                    <td>Horse Power</td>
                    <td>${car.horsePower}</td>
                </tr>
                <tr>
                    <td>Year</td>
                    <td>${car.year}</td>
                </tr>
                <tr>
                    <td>Fuel Type</td>
                    <td>${car.fuelType}</td>
                </tr>
                <tr>
                    <td>Transmission Type</td>
                    <td>${car.transmissionType}</td>
                </tr>
                <tr>
                    <td>Differential Type</td>
                    <td>${car.differentialType}</td>
                </tr>
                <tr>
                    <td>Engine Volume</td>
                    <td>${car.engineVolume} sm3</td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td>${car.price} ${car.currency}</td>
                </tr>
                <tr>
                    <td>Rent Type</td>
                    <td><%= ((Car) request.getAttribute("car")).getRentType().toString()%></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <c:choose>
                        <c:when test="${car.status eq 'AVAILABLE'}">
                            <td style="color: green">Available</td>
                        </c:when>
                        <c:when test="${car.status eq 'AT_RENT' || (rent ne 'NULL' && rent.status eq 'REQUESTED_FOR_RENT')}">
                            <td style="color: red">At rent</td>
                        </c:when>
                    </c:choose>

                </tr>
            </tbody>
        </table>
        <hr/>
        <div style="height: 10px">

        </div>

    </div>
</div> 

<style>
    span {
        font-size: 19px;
        font: bold;
    }
    td{ 
        height: 30px;
        padding-left: 5px;
    }
    #rentnow:hover{
        cursor: pointer;
    }
</style>

<script>
    $(document).ready(function () {
        $('.bxslider').bxSlider({
            mode: 'fade',
            auto: true,
            autoControls: true
        });
    });
    <c:if test="${sessionScope.isLogged}">
    $("#rentnow").on("click", function () {
        $("#rentingDetails").toggle("slow", function () {
            if ($("#rentnow").text() != "Pending..")
                if ($("#rentnow").text() != "Cancel")
                    $("#rentnow").text("Cancel");
                else
                    $("#rentnow").text("Rent Now !");
        });
    });
    $("#rentrequest").on("click", function () {
        if ($("#rentrequest").text() != "Cancel Request") {
            var result = confirm("Are you sure to rent?");
            if (result) {
                $("#rentingDetails").toggle("slow", function () {
                    $.post("RentServlet", {action: "rent", car_id: ${requestScope.car.id}}).done(function (data) {
                        var success = JSON.parse(data);
                        alert(success.success);
                        $("#rentnow").text("Pending..");
                        $("#waiting").show("slow");
                    }).fail(function () {
                        alert("error");
                    });
                });
                $("#rentrequest").text("Cancel Request")
            }
        } else {
            var result = confirm("Are you sure to cancel?");
            if (result) {
                $("#rentingDetails").toggle("slow", function () {
                    $.post("RentServlet", {action: "cancel", car_id: ${requestScope.car.id}}).done(function (data) {
                        var success = JSON.parse(data);
                        alert(success.success);
                        $("#rentnow").text("Rent Now !");
                        $("#waiting").hide("slow");
                    }).fail(function () {
                        alert("error");
                    });
                });
                $("#rentrequest").text("Request for rent")
            }
        }
    });
    $("#rentAccepted").on("click", function () {
        $("#rentInformation").toggle("slow", function () {

        });
    });
        <c:if test="${requestScope.rentee.id > 0}">
    $("#finishProcess").on("click", function () {
        $.post("RentServlet", {action: "finishRentProcess", rentee_id: ${requestScope.rentee.id}, car_id: ${requestScope.car.id}}).done(function (data) {
            var success = JSON.parse(data);
            if (success.success) {
                $("#renterInformation").hide();
            }
            $("#rentnow").text("Rent Now !");
            $("#waiting").hide("slow");
        }).fail(function () {
            alert("error");
        });
    });
        </c:if>
    </c:if>
</script>