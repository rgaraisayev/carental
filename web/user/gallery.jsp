<%-- 
    Document   : gallery
    Created on : Mar 12, 2016, 11:36:20 PM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<div class="fix single_content floatleft">
    <img src="images/gal.jpg" alt=""/>
    <div class="fix single_content_info">
        <div class="fix post-meta">
            <p>Mercedes-Benz G-Class (2006) 320$ (per day)</p>
        </div>
    </div>
</div>

<div class="fix single_content floatleft">
    <img src="images/santa-fe.jpg" alt=""/>
    <div class="fix single_content_info">
        <div class="fix post-meta">
            <p>Hyundai Santa-Fe (2010) 200$ (per day)</p>
        </div>
    </div>
</div>

<div class="fix single_content floatleft">
    <img src="images/sls63.jpg" alt=""/>
    <div class="fix single_content_info">
        <div class="fix post-meta">
            <p>Mercedes SLS63 (2015) 550$ (per day)</p>
        </div>
    </div>
</div>

<div class="fix single_content floatleft">
    <img src="images/jeep.jpg" alt=""/>
    <div class="fix single_content_info">
        <div class="fix post-meta">
            <p>Jeep Renegade (2015) 500$ (per day)</p>
        </div>
    </div>
</div>

<div class="fix single_content floatleft">
    <img src="images/prado.jpg" alt=""/>
    <div class="fix single_content_info">
        <div class="fix post-meta">
            <p>Toyota Prado (2014) 100$ (per day)</p>
        </div>
    </div>
</div>

<div class="fix single_content floatleft">
    <img src="images/q5.jpg" alt=""/>
    <div class="fix single_content_info">
        <div class="fix post-meta">
            <p>Audi Q5 (2013) 400$ (per day)</p>
        </div>
    </div>
</div>
