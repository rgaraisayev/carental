<%-- 
    Document   : index
    Created on : Feb 19, 2016, 7:33:21 PM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>

<%@page import="az.edu.ada.carental.enums.SessionKeys"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

    <head>
        <title>Home-CaRental</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/user/font/font.css"/>
        <link href="${pageContext.request.contextPath}/user/style.css" rel="stylesheet" media="screen"/>	
        <link href="${pageContext.request.contextPath}/user/responsive.css" rel="stylesheet" media="screen"/>	
        <script src="http://code.jquery.com/jquery.js"></script>	
<!--        <script src="${pageContext.request.contextPath}/user/css/dropdown/dropdown.js"></script>	
        <script src="${pageContext.request.contextPath}/user/css/dropdown/dropdown.min.js"></script>	
        <script src="${pageContext.request.contextPath}/user/css/dropdown/index.js"></script>	
        <link href="${pageContext.request.contextPath}/user/css/dropdown/dropdown.css"/>
        <link href="${pageContext.request.contextPath}/user/css/dropdown/dropdown.min.css"/>-->
    </head>


    <body >
        <div class="fix header_area">
            <div class="fix wrap header">
                <div class="logo floatleft">
                    <img width="215px" style="margin-top: 8px" height="20px"src="${pageContext.request.contextPath}/user/images/logo.png"/>
                </div>
                <div class="manu floatright">
                    <ul id="nav-top">
                        <li><a href="${pageContext.request.contextPath}/index?page=home">Home</a></li>
                            <% if (session != null && session.getAttribute(SessionKeys.IS_LOGGED) != null && session.getAttribute(SessionKeys.LOGGED_USER) != null) {%>
                        <li><a href="${pageContext.request.contextPath}/index?page=rentyourcar">Rent Your Car</a></li>
                        <li><a href="${pageContext.request.contextPath}/index?page=profile">Profile</a></li>
                        <li><a href="${pageContext.request.contextPath}/index?page=logout">Log Out</a></li>
                            <%} else if (!(session != null && session.getAttribute(SessionKeys.IS_LOGGED) != null && session.getAttribute(SessionKeys.LOGGED_USER) != null)) { %>
                        <li><a href="${pageContext.request.contextPath}/index?page=login">Login</a></li>
                            <%if (session != null) {
                                        session.setAttribute(SessionKeys.IS_LOGGED, false);
                                    }
                                }%>
                        <li><a href="${pageContext.request.contextPath}/index?page=contact">Contact</a></li>
                        <li><a href="${pageContext.request.contextPath}/index?page=aboutus">About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="fix content_area" style="padding-top: 45px">				

            <div class="fix wrap content_wrapper">
                <div class="fix content">
                    <div class="fix main_content floatleft">
                        <div id="items" class="fix single_content_wrapper">
                            <!--          Here comes all       -->
                            <c:choose>
                                <c:when test="${requestScope.page eq 'contact'}">
                                    <%@include file="contact.jsp" %>
                                </c:when>
                                <c:when test="${requestScope.page eq 'home'}">
                                    <%@include file="home.jsp" %>
                                </c:when>
                                <c:when test="${requestScope.page eq 'aboutus'}">
                                    <%@include file="about.jsp" %>
                                </c:when>
                                <c:when test="${requestScope.page eq 'rentyourcar'}">
                                    <%@include file="rentyourcar.jsp" %>
                                </c:when>
                                <c:when test="${requestScope.page eq 'login'}">
                                    <%@include file="login.jsp" %>
                                </c:when>
                                <c:when test="${requestScope.page eq 'details'}">
                                    <%@include file="details.jsp" %>
                                </c:when>
                                <c:when test="${requestScope.page eq 'requestReview'}">
                                    <%@include file="requestReview.jsp" %>
                                </c:when>
                                <c:when test="${requestScope.page eq 'register'}">
                                    <%@include file="register.jsp" %>
                                </c:when>
                                <c:when test="${requestScope.page eq 'profile'}">
                                    <%@include file="profile.jsp" %>
                                </c:when>
                            </c:choose>
                        </div>
                    </div>
                    <div class="fix sidebar floatright"  >
                        <div class="fix single_sidebar" >
                            <div class="fix single_sidebar" >
                                <h2>Search</h2>
                                <div><button  id="search" style="float: right; padding: 1px; margin: 8px 3px 0px 3px;" ><img width="25px" height="25px" src="https://cdn3.iconfinder.com/data/icons/web-and-internet-icons/512/Magnifying_Glass-512.png"/></button></div>
                                <div id="filter" style="margin-top: 50px;">
                                    <label style="margin-right: 10px" >Price:</label><input id="min" type="number" style="width: 60px" min="1" placeholder="min" /><input id="max" type="number" style="width: 60px; margin-left: 5px"  placeholder="max"/><br/>
                                    <label style="margin-right: 10px; margin-top: 35px" >Status:</label>
                                    <select id="status" style="margin-top: 35px; padding: 3px">
                                        <option value="AT_RENT">At rent</option>
                                        <option value="AVAILABLE">Available</option>
                                        <option value="ANY">Any</option>
                                    </select><br/>
                                    <label style="margin-right: 10px; margin-top: 35px" >Brand:</label>
                                    <select id="brand_index" name="brand_index" style="margin-top: 35px; padding: 3px">
                                        <c:forEach items="${requestScope.brands}" var="brand">
                                            <option value="${brand.name}">${brand.name}</option>
                                        </c:forEach>
                                    </select>
                                    <select   id="model_index"  name="model_index" style="margin-top: 35px; padding: 3px">
                                    </select>
                                </div>
                            </div>
                            <div class="popular_post fix">
                                <h2>Last added</h2>
                                <c:forEach items="${requestScope.last}" var="car">
                                    <div class="fix single_popular">
                                        <a href="index?page=details&carId=${car.id}"><img width="80px" height="80px" src="${car.imageUrl}" class="floatleft"/>
                                            <h2 style="font-size: 17px">${car.model.brand.name} ${car.model.name}</h2>
                                            <p style="font-size: 13px; color: red">${car.price} ${car.currency}</p></a>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fix footer_area">
            <div class="wrap">
                <div class="fix copyright_text floatleft">
                    <p>IFT-200 CaRental Project</p>
                </div>
                <div class="fix social_area floatright">
                    <ul>
                        <li><a href="" class="feed"></a></li>
                        <li><a href="" class="facebook"></a></li>
                        <li><a href="" class="twitter"></a></li>
                        <li><a href="" class="drible"></a></li>
                        <li><a href="" class="flickr"></a></li>
                        <li><a href="" class="pin"></a></li>
                        <li><a href="" class="tumblr"></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/placeholder_support_IE.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/selectnav.min.js"></script>
        <script type="text/javascript">
            selectnav('nav-top', {
                label: '-Navigation-',
                nested: true,
                indent: '-'
            });
            selectnav('nav-bottom', {
                label: '-Navigation-',
                nested: true,
                indent: '-'
            });

            $("#search").on("click", function () {
                $("#items").text("");
                $.get("SearchServlet", {min: $("#min").val(), max: $("#max").val(), status: $("#status").val(), brand: $("#brand").val(), model: $("#model").val()
                }).done(function (data) {
                    var htmlText = "";
                    var d = JSON.parse(data);
                    $.each(d, function (index, car) {
                        htmlText += " <div class=\"fix single_content floatleft\">\n" +
                                "            <img src=\"" + car.imageUrl + "\" alt=\"Car Image\"/>\n" +
                                "            <div class=\"fix single_content_info\">\n" +
                                "                <h1 style=\"color:black\">" + car.brand + " " + car.model + "</h1>\n" +
                                "                <h4 style=\"color:#888\">" + car.description + "</h4>\n" +
                                "                        <div>Status <span style=\"color: green\">" + car.status + "</span></div>\n" +
                                "                <div class=\"fix post-meta\">\n" +
                                "                    <a  href=\"index?page=details&carId=" + car.id + "\"><p style=\"color: blue; font-size: 14px\">Click for more information</p></a>\n" +
                                "                </div>\n" +
                                "            </div>\n" +
                                "        </div> ";
                    });
                    if (htmlText == "" && $("#items").html() == "")
                        htmlText = "<div style='padding-left: 50%; height: 40px; width: 100%; background-color: grey; color: white'> Data is not found </div";
                    $("#items").append(htmlText);
                });
            });

            $(document).on("ready", function () {
                getModels();
            });

            $("#brand_index").change(function () {
                getModels();
            });

            function getModels() {
                $("#model_index").html("");
                $.get("RentYourCar", {name: $("#brand_index option:selected").text()}, function (data) {
                    var models = JSON.parse(data);
                    var options = "";
                    $.each(models, function (index, model) {
                        options += "<option value=" + model.name + ">" + model.name + "</option>";
                    });
                    $("#model_index").append(options);
                });
            }



        </script>			
    </body>
</html>

