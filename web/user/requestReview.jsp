
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/jquery.bxslider.css"/>
<script src="${pageContext.request.contextPath}/user/js/jquery.bxslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/jquery.bxslider.min.js"></script>
<div class="single_page_content fix">
    <div class="fix single_content_info">       
        <div id="rentingDetails" style="padding: 0px 0px 40px 3px">
            <div id="result" style=" display: none; width: 100%; height: 40px; text-align: center;padding-top: 20px;background-color: #d6e9c6"><p style="color: #0A0A0A; font-size: 17px">Congratulations! Now you can wait his call</p></div>
            <h4 style="color:#000">Rentee Information</h4>
            <p style="color:#000">Name: ${rentee.name} ${rentee.surname}</p>
            <p style="color:#000">Phone: ${rentee.phone}</p>
            <p style="color:#000">Email: ${rentee.email}</p>
            <button id="acceptRequest" style="margin-top: 10px; padding: 4px">Accept Request</button>
            <button id="cancelRequest" style="margin-top: 10px; padding: 4px">Cancel Request</button>
        </div>
        <hr/>
        <div style="height: 10px">

        </div>

    </div>
</div> 

<script>
    $(document).ready(function () {
        $('.bxslider').bxSlider({
            mode: 'fade',
            auto: true,
            autoControls: true
        });
    });
    <c:if test="${sessionScope.isLogged}">

    $("#cancelRequest").on("click", function () {
        var result = confirm("Are you sure to cancel?");
        if (result) {
            $.post("RentServlet", {action: "cancelRent", rentee_id: ${requestScope.rentee.id}, car_id: ${requestScope.rent.carId}}).done(function (data) {
                var success = JSON.parse(data);
                $("#result").text("Request is Canceled, You can undo before leaving");
                $("#result").show("slow");
                 $("#acceptRequest").show();
                $("#cancelRequest").hide();
            }).fail(function () {
                alert("error");
            });
            $("#rentrequest").text("Cancel Request");
        }
    });
    $("#acceptRequest").on("click", function () {
        var result = confirm("Are you sure to accept?");
        if (result) {
            $("#result").toggle("slow", function () {
                $.post("RentServlet", {action: "acceptRent", rentee_id:${requestScope.rentee.id}, car_id: ${requestScope.rent.carId}}).done(function (data) {
                    var success = JSON.parse(data);
                    if (success.success)
                        $("#acceptRequest").hide();
                    else   alert("Something is going wrong");
                }).fail(function () {
                    alert("error");
                });

            });
            $("#rentrequest").text("Accept Request");
        }
    });
    </c:if>
    <c:if test="${!sessionScope.isLogged}">
    alert("You have to login");

    </c:if>
</script>