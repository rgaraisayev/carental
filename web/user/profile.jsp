<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="${pageContext.request.contextPath}/user/js/intlTelInput.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/intlTelInput.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/help-tips.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/jss/validation.js"/>
<div class="single_page_content fix">
    <h1 style="margin-bottom:15px;">Profile</h1><br>
    <div class="contact_form">
        <center><img  id="image" src="http://www.gravatar.com/avatar/f97018026f7a8af74b04b5ce22733cc7?s=230&r=g&d=mm" width=170px; height=170px;/></center>
        <!--<center> <input class="image" type="file" value="Change Picture" name="profImage" accept="image/x-png, image/gif, image/jpeg"  /></center>-->
        <div id="editLayout">
            <center><h4><input  class="text"  id="editName" type="text" name="name" value="${sessionScope.loggedUser.name}"/></h4></center>
            <center><h4><input  class="text"  id="editSurname" type="text" name="surname" value="${sessionScope.loggedUser.surname}"/></h4></center>
            <!--<center><h4><input  class="text"  id="editEmail" type="email" name="email" value="${sessionScope.loggedUser.email}"/></h4></center>-->
            <h5  class="text"  style=" width: 650px;  margin-bottom: 10px; margin-left: 116px"><input id="phone"  class="text"  type="tel" name="phone" value="${sessionScope.loggedUser.phone}"  onkeydown="return checkNumber(event)" placeholder="Phone number"/></h5>
            <center> <button id="save" >Save Changes</button>
        </div> 
        <div id="profileInfo">
            <center> <a onclick="editProfile()"><img width="20px" height="20px" src="https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/512/edit_user-.png"/></a></center>
            <center><h1 id="nameSurname"> ${sessionScope.loggedUser.name} ${sessionScope.loggedUser.surname}</h1>  </center>
            <center><h4 id="phone2">${sessionScope.loggedUser.phone}</h4></center><br/>
        </div>
        <center><h4 id="email"><i> ${sessionScope.loggedUser.email} </i></h4></center>
        <hr/>
        <center><h1>Pending rent requests</h1></center><br/>
        <ul>
            <c:forEach items="${requestScope.requested}" var="rent">
                <form id="requestForm" action="index" method="post">
                    <input type="hidden" name="page" value="requestReview"/>
                    <input type="hidden" name="rentee_id" value="${rent.renteeId}"/>
                    <input type="hidden" name="renter_id" value="${rent.renterId}"/>
                    <input type="hidden" name="car_id" value="${rent.carId}"/>
                    <input style="display: none" type="submit" value="Submit">
                </form>
                <li><a onclick="submitRequestForm()"><img src="${rent.car.imageUrl}" width=145; height=150/></a>
                <center><p>${rent.car.model.brand.name} ${rent.car.model.name}</p></center></li>
                </c:forEach>
        </ul>
        <c:choose>
            <c:when test="${empty requestScope.requested}">
                <center><p>No request</p></center>
                </c:when>
                <c:otherwise>
                <center><a href="${pageContext.request.contextPath}/index?page=home&user=${sessionScope.loggedUser.id}" style="color: blue; text-decoration: underline">View all</a></center>
                </c:otherwise>
            </c:choose><hr/>
        <center><h1> Your cars </h1></center><br/>

        <ul>
            <c:forEach items="${requestScope.cars}" var="car">
                <li><a href="index?page=details&carId=${car.id}"><img src="${car.imageUrl}" width=145; height=150/></a>
                <center><p>${car.model.brand.name} ${car.model.name}</p></center></li>
                </c:forEach>
        </ul>
        <c:choose>
            <c:when test="${empty requestScope.cars}">
                <center><p>No cars</p></center>
                </c:when>
                <c:otherwise>
                <center><a href="${pageContext.request.contextPath}/index?page=home&user=${sessionScope.loggedUser.id}" style="color: blue; text-decoration: underline">View all</a></center>
                </c:otherwise>
            </c:choose>
        <hr/>
        <center><h1> Your rent history </h1></center><br/>
        <ul >
            <c:forEach items="${requestScope.history}" var="rent">
                <li><a href="index?page=details&carId=${rent.carId}"><img src="${rent.car.imageUrl}" width=145; height=150/></a>
                <center><p>${rent.car.model.brand.name} ${rent.car.model.name}</p></center></li>
                </c:forEach>
        </ul>
        <c:choose>
            <c:when test="${empty requestScope.history}">
                <center><p>No cars</p></center>
                </c:when>
                <c:otherwise>
                <center><a href="#" style="color: blue; text-decoration: underline">View all</a></center>
                </c:otherwise>
            </c:choose>
        <hr/>
        <center><h1> Currently at rent </h1></center><br/>
        <ul >
            <c:forEach items="${requestScope.current}" var="rent">
                <li><a href="index?page=details&carId=${rent.carId}"><img src="${rent.car.imageUrl}" width=145; height=150/></a>
                <center><p>${rent.car.model.brand.name} ${rent.car.model.name}</p></center></li>
                </c:forEach>
        </ul>
        <c:choose>
            <c:when test="${empty requestScope.current}">
                <center><p>No cars</p></center>
                </c:when>
                <c:otherwise>
                <center><a href="#" style="color: blue; text-decoration: underline">View all</a></center>
                </c:otherwise>
            </c:choose>
        <hr/>
    </div>
</div>


<style>
    ul{
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
    }
    li {
        float: left;
    }

    .image{
        padding: 5px;
        margin-top: 10px;
    }
    #editEmail{
        padding: 6px;
    }

    #editLayout{
        display: none;   
    }

    #save{
        padding: 10px;
        margin-bottom: 10px; 
    }

</style>

<script>

    $(document).ready(function () {
        $("#phone").intlTelInput({
            onlyCountries: ["az"]
        });
    });

    function submitRequestForm() {
        $("#requestForm").submit();
    }

    function editProfile() {
        $("#editLayout").show("slow");
        $("#profileInfo").hide("slow");
    }

    $("#save").on("click", function () {
        if ($("#editName").val() != "" && $("#editSurname").val() != "" && $("#phone").intlTelInput("getNumber") != "" && $("#phone").intlTelInput("getNumber") != "+994") {
            $.post("ProfileServlet", {name: $("#editName").val(), surname: $("#editSurname").val(), phone: $("#phone").intlTelInput("getNumber")}, function (data) {
                var suc = JSON.parse(data);
                if (suc.success) {
                    $("#nameSurname").text($("#editName").val() + " " + $("#editSurname").val());
                    $("#phone2").text($("#phone").intlTelInput("getNumber"));
                    $("#editLayout").hide("slow");
                    $("#profileInfo").show("slow");
                } else {
                    alert("Error Occured, try again");
                }
            });
        } else {
            alert("Please fill the blanks");
            return false;
        }

    });

</script>