/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function checkNumber(event) {
    return event.which >= 48 && event.which <= 57 || event.which == 8 || event.which == 46 || event.which == 37 || event.which == 39;
}

function validateEmail() {
    var email = $("#email").val();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(email)) {
        $("#emailError").html("Not valid email");
    } else {
        $("#emailError").html("");
    }
    return re.test(email);
}

function passwordStrongnessChecker() {
    var pass = $("#password").val();
    var array = ["&", "*", "$", "@", "!", "#", "%", "^", "(", "{", "}", ")", "`", "~", "/", ".", ",", ":", "?", "+", "-", ";"];
    var containsCount = 0;
    for (var i = 0; i < array.length; i++) {
        if (pass.indexOf(array[i]) > -1) {
            containsCount++;
        }
    }
    if (pass.match(/\d+/g) != null) {
        containsCount++;
    }
    if (pass.length >= 6 && containsCount >= 3) {
        //  very strong
        $("#strongPass").css({"color": "green"});
        $("#strongPass").text("Very Strong");

    } else if (pass.length > 4 && containsCount >= 2) {
        // strong
        $("#strongPass").css({"color": "grey"});
        $("#strongPass").text("Strong");
    } else if ((pass.length > 4 && containsCount > 0) || pass.length > 8) {
        // good
        $("#strongPass").css({"color": "blue"});
        $("#strongPass").text("Good");
    } else {
        // bad
        $("#strongPass").css({"color": "red"});
        $("#strongPass").text("Bad password");
        return false;
    }
    return true;
}

function passwordEqualityChecker() {
    if ($("#password").val() != $("#password2").val()) {
        $("#equalPass").css("color", "red");
        $("#equalPass").text("Passwords are not equal");
        return false;
    } else {
        $("#equalPass").text("");
    }
    return true;
}

function checkBlanks() {
    if ($("#name").val() != "" && $("#surname").val() != "" && $("#phone").val() != "") {
        $("#blanks").html("");
        return true;
    } else {
        $("#blanks").html("Fill all the blanks");
    }
}