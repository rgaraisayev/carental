<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach items="${requestScope.cars}" var="car">
   
        <div class="fix single_content floatleft">
            <img src="${car.imageUrl}" alt="Car Image"/>
            <div class="fix single_content_info">
                <h1 style="color:black">${car.model.brand.name}  ${car.model.name}</h1>
                <h4 style="color:#888">${car.description}</h4>
                <c:choose>
                    <c:when test="${car.status eq 'AVAILABLE'}">
                        <div>Status <span style="color: green">Available</span></div>
                    </c:when>
                         <c:when test="${car.status eq 'AT_RENT'}">
                        <div>Status <span style="color: red">At rent</span></div>
                    </c:when>
                </c:choose>
                <div class="fix post-meta">
                    <a  href="index?page=details&carId=${car.id}"><p style="color: blue; font-size: 14px">Click for more information</p></a>
                </div>
            </div>
        </div> 
</c:forEach>