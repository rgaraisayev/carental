<%-- 
    Document   : about
    Created on : Mar 12, 2016, 11:37:34 PM
    Author     : Ramazan Garaisayev
    Email      : rgaraisayev2018@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<div class="single_page_content fix">
    <h1>About Us</h1><br/>
    <img src="${pageContext.request.contextPath}/user/images/students.jpg" class="single_feature_img" alt=""/>
    <p>We are a group of undergraduate students who are currently studying at ADA University. Our major is Information Technologies and hopefully we will be the first graduates from this major in 2018. For some of us this project is the first one that was done completely. However, in our rows we have members who are experienced a lot with this kind of project and established their own small business named "Unibyte". </p>

    <br/><blockquote>Our group consists of web-designer, front-end and back-end developers. Leyla Jafarli, Ramazan Garaisayev, Murad Huseynli, Rasim Mollayev and Buludhkan Alizade are the members of this "project-based" group</blockquote><br/>
    <h2>About CaRental</h2>
    <p>CaRental is a group project that was done by the indicated students for IFT-200 "New Media Design Digital Survey" course. Obviously, this is a website where users can easily rent cars or place their cars to be rented.</p>

</div>
