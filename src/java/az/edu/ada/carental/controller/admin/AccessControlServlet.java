/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.admin;

import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.ErrorTypes;
import az.edu.ada.carental.enums.UserType;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ramco
 */
public class AccessControlServlet extends HttpServlet {

    private String aName;
    private String aPassword;

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        aName = getServletConfig().getInitParameter("adminUsername");
        aPassword = getServletConfig().getInitParameter("adminPassword");

    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        if (action != null && action.equals("logout")) {
            request.getSession().invalidate();
            response.sendRedirect("adminPanel/login.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = (String) request.getParameter("name");
        String password = (String) request.getParameter("password");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("adminPanel/login.jsp");
        if (aName != null && aPassword != null) {
            if (password != null && name != null) {
                if (name.equals(aName) && password.equals(aPassword)) {
                    HttpSession session = request.getSession();
                    session.setAttribute("admin", UserType.ADMIN);
                    session.setAttribute("name", name);
                    SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    getServletContext().setAttribute("lastAccessedTime", sm.format(new Date()));
                    String lastAccessedTime = (String) getServletContext().getAttribute("lastAccessedTime");
                    String currentDate = sm.format(new Date());
                    if (lastAccessedTime != null && !lastAccessedTime.equals("")) {
                        session.setAttribute("lastAccessedTime", lastAccessedTime);
                    } else {
                        session.setAttribute("lastAccessedTime", currentDate);
                    }
                    /* update Time */
                    getServletContext().setAttribute("lastAdminAccessTime", currentDate);

                    response.sendRedirect("main");
                } else {
                    // not same cridentials
                    request.setAttribute("name", name);
                    request.setAttribute("error", ErrorTypes.WRONG_CREDENTIALS);
                    requestDispatcher.forward(request, response);
                }
            } else {
                // blank cridentials
                request.setAttribute("error", ErrorTypes.BLANK_CREDENTIALS);
                requestDispatcher.forward(request, response);
            }
        } else {
            // error initParams
            request.setAttribute("name", name);
            request.setAttribute("error", ErrorTypes.ERROR_IN_WEB_XML);
            requestDispatcher.forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
