/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.admin.tables;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.dao.RequestDAO;
import az.edu.ada.carental.dao.UserDAO;
import az.edu.ada.carental.entities.Request;
import az.edu.ada.carental.enums.Navigation;
import az.edu.ada.carental.enums.UserType;
import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import az.edu.ada.carental.utility.search.searchEngine.FullSearch;
import az.edu.ada.carental.utility.search.searchEngine.condinments.common.Limit;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 *
 * @author ramco
 */
public class DataTablesServlet extends HttpServlet {

    private CarDAO carD;
    private UserDAO userD;
    private RequestDAO requestD;

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        carD = CarDAO.getInstance();
        userD = UserDAO.getInstance();
        requestD = RequestDAO.getInstance();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Session check */
        HttpSession session = request.getSession();
        if (session == null) {
            response.sendRedirect("adminPanel/login.jsp");
            return;
        } else if (session.getAttribute("admin") != UserType.ADMIN) {
            response.sendRedirect("adminPanel/login.jsp");
            return;
        }

        /* Choose Table */
        AbstractSearchEngine searchEngine;
        String table = request.getParameter("table");
        int begin = Integer.parseInt(request.getParameter("begin"));
        int end = Integer.parseInt(request.getParameter("end"));
        searchEngine = new FullSearch(table, RequestDAO.C);
        searchEngine = new Limit(searchEngine, begin, end);
        response.setContentType("application/json");
        response.setHeader("Cache-Control", "no-store");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        JSONObject jSONObject = requestD.advancedSearch(searchEngine);
        System.out.println(jSONObject.toJSONString());
        out.print(jSONObject.toJSONString());
//        System.out.println(json);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
