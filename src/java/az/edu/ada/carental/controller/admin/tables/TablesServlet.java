/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.admin.tables;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.dao.RequestDAO;
import az.edu.ada.carental.dao.UserDAO;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.Navigation;
import az.edu.ada.carental.enums.UserType;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ramco
 */
public class TablesServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Session check */
        HttpSession session = request.getSession();
        if (session == null) {
            response.sendRedirect("adminPanel/login.jsp");
            return;
        } else if (session.getAttribute("admin") != UserType.ADMIN) {
            response.sendRedirect("adminPanel/login.jsp");
            return;
        }
        String tables = request.getParameter("tables");
        if (tables != null) {
            if (tables.equals("all")) {
                request.setAttribute("navigation", Navigation.TABLE_CHOOSE);
                request.getRequestDispatcher("adminPanel/index.jsp").forward(request, response);
            } else if (tables.equals("users")) {
                UserDAO userD = UserDAO.getInstance();
                request.setAttribute("userTableC", UserDAO.C);

                /* data*/
                request.setAttribute("users", userD.selectAll());

                request.setAttribute("navigation", Navigation.TABLES);
                request.setAttribute("table", tables);
                request.getRequestDispatcher("adminPanel/index.jsp").forward(request, response);
            } else if (tables.equals("cars")) {
                CarDAO carD = CarDAO.getInstance();

                /* Columns */
                request.setAttribute("carTableC", CarDAO.C);

                /* data*/
                request.setAttribute("cars", carD.selectAll(true, -1, -1, request, CarStatus.ANY));

                request.setAttribute("navigation", Navigation.TABLES);
                request.setAttribute("table", tables);
                request.getRequestDispatcher("adminPanel/index.jsp").forward(request, response);
            } else if (tables.equals("requests")) {
                RequestDAO requestD = RequestDAO.getInstance();

                /* Columns */
                request.setAttribute("requestTableC", RequestDAO.C);

                /* Data */
                request.setAttribute("requests", requestD.selectAll());

                request.setAttribute("navigation", Navigation.TABLES);
                request.setAttribute("table", tables);
                request.getRequestDispatcher("adminPanel/index.jsp").forward(request, response);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
