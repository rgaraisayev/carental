/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.admin;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.dao.RequestDAO;
import az.edu.ada.carental.dao.UserDAO;
import az.edu.ada.carental.entities.Request;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.Navigation;
import az.edu.ada.carental.enums.RequestStatus;
import az.edu.ada.carental.enums.UserType;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ramco
 */
public class RequestsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /* Session check */
        HttpSession session = request.getSession();
        if (session == null) {
            response.sendRedirect("adminPanel/login.jsp");
            return;
        } else if (session.getAttribute("admin") != UserType.ADMIN) {
            response.sendRedirect("adminPanel/login.jsp");
            return;
        }

        String status = request.getParameter("status");

        if (status != null) {
            List<Request> requests;
            RequestDAO requestDAO = RequestDAO.getInstance();
            if (status.equals("accepted")) {
                requests = requestDAO.selectRequestsByStatus(RequestStatus.ACCEPTED, request);
                request.setAttribute("status", RequestStatus.ACCEPTED);
            } else if (status.equals("rejected")) {
                requests = requestDAO.selectRequestsByStatus(RequestStatus.REJECTED, request);
                request.setAttribute("status", RequestStatus.REJECTED);
            } else if (status.equals("pending")) {
                requests = requestDAO.selectRequestsByStatus(RequestStatus.PENDING, request);
                request.setAttribute("status", RequestStatus.PENDING);
            } else {
                response.sendRedirect("main");
                return;
            }
            request.setAttribute("requests", requests);
            request.setAttribute("navigation", Navigation.STATUS_VIEW);
            request.getRequestDispatcher("adminPanel/index.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String attemp = request.getParameter("attemp");
        String id = request.getParameter("id");
        String carId = request.getParameter("car_id");
        PrintWriter out = response.getWriter();
        RequestDAO requestDAO = RequestDAO.getInstance();
        if (attemp != null && id != null) {
            try {
                long idValue = Long.parseLong(id);
                int carID = Integer.parseInt(carId);
                if (attemp.equals("reject")) {
                    requestDAO.updateStatusById(idValue, RequestStatus.REJECTED);
                    CarDAO.getInstance().updateStatusById(carID, CarStatus.NOT_AVAILABLE);
                } else if (attemp.equals("accept")) {
                    requestDAO.updateStatusById(idValue, RequestStatus.ACCEPTED);
                    CarDAO.getInstance().updateStatusById(carID, CarStatus.AVAILABLE);
                }
            } catch (Exception e) {
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
