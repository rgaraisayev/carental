/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.admin;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.dao.RequestDAO;
import az.edu.ada.carental.dao.UserDAO;
import az.edu.ada.carental.entities.Car;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.Navigation;
import az.edu.ada.carental.enums.RequestStatus;
import az.edu.ada.carental.enums.UserType;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ramco
 */
public class MainPageServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /* Session check */
        HttpSession session = request.getSession();
        if (session == null) {
            response.sendRedirect("adminPanel/login.jsp");
            return;
        } else if (session.getAttribute("admin") != UserType.ADMIN) {
            response.sendRedirect("adminPanel/login.jsp");
            return;
        }
        System.out.println("here");

        CarDAO carD = CarDAO.getInstance();
        UserDAO userD = UserDAO.getInstance();
        RequestDAO requestDAO = RequestDAO.getInstance();

        /* ACCEPTED/REJECTED/PENDING Requests BEGIN*/
        long acceptedRequestCount = requestDAO.selectRequestCountByStatus(RequestStatus.ACCEPTED);
        long rejectedRequestCount = requestDAO.selectRequestCountByStatus(RequestStatus.REJECTED);
        long pendingRequestCount = requestDAO.selectRequestCountByStatus(RequestStatus.PENDING);

        request.setAttribute("acceptedRequestCount", acceptedRequestCount);
        request.setAttribute("rejectedRequestCount", rejectedRequestCount);
        request.setAttribute("pendingRequestCount", pendingRequestCount);
        /* Requests END */

 /* Statistics  BEGIN */
        // Guests
        Long guests = (Long) getServletContext().getAttribute("guests");
        if (guests != null) {
            request.setAttribute("guests", guests);
        } else {
            request.setAttribute("guests", (long) 0);
        }
        // Online users
        Long onlineUsers = (Long) getServletContext().getAttribute("onlineUsers");
        if (onlineUsers != null) {
            request.setAttribute("onlineUsers", onlineUsers);
        } else {
            request.setAttribute("onlineUsers", (long) 0);
        }
        //All Users 
        long userCount = userD.selectUserCount();
        // All requests
        long requestCount = requestDAO.selectRequestCount();
        // Request per User
        long RPU = 0;
        if (userCount != 0) {
            RPU = requestCount / userCount;
        }
        request.setAttribute("RPU", RPU);

        /* Statistics END */
        request.setAttribute("navigation", Navigation.MAIN);
        request.getRequestDispatcher("adminPanel/index.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
