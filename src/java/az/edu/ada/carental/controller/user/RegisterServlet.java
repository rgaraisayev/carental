/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.user;

import az.edu.ada.carental.dao.UserDAO;
import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.UserStatus;
import az.edu.ada.carental.enums.UserType;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ramco
 */
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO userDAO = UserDAO.getInstance();
        String email = (String) request.getParameter("email");
        String password = (String) request.getParameter("password");
        String password2 = (String) request.getParameter("password2");
        String name = (String) request.getParameter("name");
        String surname = (String) request.getParameter("surname");
        String phone = (String) request.getParameter("phone");
        System.out.println( " Phone " + phone);
        request.setAttribute("phone", phone);
        request.setAttribute("surname", surname);
        request.setAttribute("name", name);
        request.setAttribute("email", email);
        if (email != null && password != null && password2 != null && name != null && surname != null && phone != null) {
            if (!email.equals("") && !password.equals("") && !password2.equals("") && !name.equals("") && !surname.equals("") && !phone.equals("")) {
                if (password.equals(password2)) {
                    if (!userDAO.isUserExistByEmail(email)) {
                        User newUser = new User();
                        newUser.setEmail(email);
                        newUser.setPassword(password);
                        newUser.setName(name);
                        newUser.setSurname(surname);
                        newUser.setPhone(phone);
                        newUser.setType(UserType.USER);
                        if (userDAO.insert(newUser)) {
                            // everything is ok
                            request.setAttribute("page", "login");
                            request.getRequestDispatcher("user/index.jsp").forward(request, response);
                        } else {
                            // error inserting user, return 
                            request.setAttribute("error", true);
                            trigger(request, response);
                        }
                    } else {
                        // username exists,  return choose another one
                        request.setAttribute("emailExists", true);
                        trigger(request, response);
                    }
                } else {
                    request.setAttribute("passDontMatch", true);
                    trigger(request, response);
                }
            } else {
                request.setAttribute("blank", true);
                trigger(request, response);
            }
        } else {
            request.setAttribute("blank", true);
            trigger(request, response);
        }

    }

    public void trigger(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("page", "register");
        request.getRequestDispatcher("user/index.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
