/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.user;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.entities.Car;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.utility.search.domen.CarSearch;
import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import az.edu.ada.carental.utility.search.searchEngine.FullSearch;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author ramco
 */
public class SearchServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CarDAO carDAO = CarDAO.getInstance();
        String min = request.getParameter("min");
        String max = request.getParameter("max");
        String status = request.getParameter("status");
        String keyword = request.getParameter("keyword");
        String model = request.getParameter("model");
        String brand = request.getParameter("brand");

        int minV = 0;
        int maxV = Integer.MAX_VALUE;
        try {
            if (!min.isEmpty()) {
                minV = Integer.parseInt(min);
                if (minV < 0) {
                    minV = 0;
                }
            }
            if (!max.isEmpty()) {
                maxV = Integer.parseInt(max);
                if (maxV <= 0) {
                    maxV = Integer.MAX_VALUE;
                }
            }
        } catch (Exception e) {
            minV = 0;
            maxV = Integer.MAX_VALUE;
        }
        // brand and model is empty, for status and min and max;
        String query;
        if (status.equals("ANY")) {
            query = "select c.id, c.status, c.imageUrl, b.name, m.name, c.description from " + CarDAO.DB_CAR_TABLE + " c join " + CarDAO.DB_MODEL_TABLE + " m on(c.model_id=m.id) join " + CarDAO.DB_BRAND_TABLE + " b on(m.brand_id=b.id) where c.status!='NOT_AVAILABLE' and c.price between " + minV + " and " + maxV;
        } else {
            query = "select c.id, c.status, c.imageUrl, b.name, m.name, c.description from " + CarDAO.DB_CAR_TABLE + " c join " + CarDAO.DB_MODEL_TABLE + " m on(c.model_id=m.id) join " + CarDAO.DB_BRAND_TABLE + " b on(m.brand_id=b.id) where c.status='" + CarStatus.valueOf(status) + "' and c.price between " + minV + " and " + maxV;

        }
        // brand is choosen

        String where = "";
        if (brand != null && !brand.isEmpty() && !brand.equals("Choose")) {
            where = " and b.name='" + brand + "'";
        }
        // model is choosen
        if (model != null && !model.isEmpty() && !model.equals("Choose")) {
            where += " and m.name='" + model + "'";
        }
        query += where;

        List<Car> cars = carDAO.getSearchResult(query);
        JSONArray array = new JSONArray();
        if (cars != null && !cars.isEmpty()) {
            for (Car car : cars) {
                JSONObject object = new JSONObject();
                object.put("id", car.getId());
                object.put("brand", car.getModel().getBrand().getName());
                object.put("model", car.getModel().getName());
                object.put("description", car.getDescription());
                object.put("imageUrl", car.getImageUrl());
                object.put("status", car.getStatus().toString());
                array.add(object);
            }
        }
        response.getWriter().print(array.toJSONString());

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
