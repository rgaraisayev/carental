/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.user;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.dao.RentDAO;
import az.edu.ada.carental.dao.UserDAO;
import az.edu.ada.carental.entities.Car;
import az.edu.ada.carental.entities.CarBrand;
import az.edu.ada.carental.entities.RentHistory;
import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.RentStatus;
import az.edu.ada.carental.enums.SessionKeys;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ramco
 */
public class MainServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page = request.getParameter("page");
        CarDAO carDAO = CarDAO.getInstance();
        HttpSession session = request.getSession();
        System.out.println("Page " + page + " Main Servlet " + session.getAttribute(SessionKeys.LOGGED_USER));
        if (page != null) {
            String u = request.getParameter("page");
            if (u != null && !u.isEmpty() && !u.contains("'") && page.equals("home")) {
                try {
                    int uID = Integer.parseInt(u);
                    request.setAttribute("cars", carDAO.selectByUserId(uID, -1, true, request, CarStatus.NOT_ANY));
                } catch (Exception e) {
                    goHome(request, carDAO);
                }
            } else if (page.equals("aboutus")) {
            } else if (page.equals("contact")) {
            } else if (page.equals("login")) {
            } else if (page.equals("rentyourcar")) {
                request.setAttribute("brands", carDAO.getAllBrands());
                List<String> cur = new ArrayList<String>();
                cur.add("AZN");
                cur.add("USD");
                request.setAttribute("currency", cur);
            } else if (page.equals("register")) {

            } else if (page.equals("requestReview")) {
                String renteeId = request.getParameter("rentee_id");
                String renterId = request.getParameter("renter_id");
                String carId = request.getParameter("car_id");
                if (!renteeId.isEmpty() && !renterId.isEmpty() && !carId.isEmpty()) {
                    try {
                        UserDAO userDAO = UserDAO.getInstance();
                        RentDAO rentDAO = RentDAO.getInstance();
                        int renteeID = Integer.parseInt(renteeId);
                        int carID = Integer.parseInt(carId);
                        User rentee = userDAO.selectById(renteeID);
                        if (rentee != null) {
                            request.setAttribute("rentee", rentee);
                            RentHistory rent = rentDAO.selectByCarIdRenteeIdAndStatus(carID, renteeID, RentStatus.REQUESTED_FOR_RENT, false);
                            if (rent != null) {
                                request.setAttribute("rent", rent);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else if (page.equals("profile")) {
                if (session.getAttribute(SessionKeys.IS_LOGGED) == null || session.getAttribute(SessionKeys.LOGGED_USER) == null) {
                    page = goHome(request, carDAO);
                } else {
                    User user = (User) session.getAttribute(SessionKeys.LOGGED_USER);
                    if (user != null) {
                        int userId = user.getId();
                        RentDAO rentDAO = RentDAO.getInstance();
                        request.setAttribute("requested", rentDAO.selectByRenterIdAndStatus(userId, RentStatus.REQUESTED_FOR_RENT, 3, request));
                        request.setAttribute("cars", carDAO.selectByUserId(userId, 3, false, request, CarStatus.NOT_ANY));
                        request.setAttribute("history", rentDAO.selectByRenteeId(userId, 3));
                        request.setAttribute("current", rentDAO.selectByRenteeIdAndStatus(userId, RentStatus.CURRENT, 3, request));
                    } else {
                        session.setAttribute(SessionKeys.IS_LOGGED, false);
                        session.setAttribute(SessionKeys.LOGGED_USER, null);
                        session.invalidate();
                        page = goHome(request, carDAO);
                    }
                }
            } else if (page.equals("logout")) {
                session.setAttribute(SessionKeys.IS_LOGGED, false);
                session.setAttribute(SessionKeys.LOGGED_USER, null);
                session.invalidate();
                page = goHome(request, carDAO);
            } else if (page.equals("details")) {
                page = goToDetails(request, page, carDAO);
            } else {
                page = goHome(request, carDAO);
            }
        } else {
            page = goHome(request, carDAO);
        }
        request.setAttribute("page", page);
        List<CarBrand> al = carDAO.getAllBrands();
        CarBrand carBrand = new CarBrand();
        carBrand.setId(-1);
        carBrand.setName("Choose");
        al.add(0, carBrand);
        request.setAttribute("brands", al);
        request.setAttribute("last", carDAO.selectAll(true, 5, -1, request, CarStatus.NOT_ANY));
        request.getRequestDispatcher("user/index.jsp").forward(request, response);

    }

    private String goToDetails(HttpServletRequest request, String page, CarDAO carDAO) {
        String carId = request.getParameter("carId");
        if (carId != null && !carId.isEmpty()) {
            try {
                User user = (User) request.getSession().getAttribute(SessionKeys.LOGGED_USER);
                int carID = Integer.parseInt(carId);
                page = "details";
                Car car = carDAO.selectById(carID, true, request, CarStatus.NOT_ANY);
                if (user != null) {
                    System.out.println("Selecting Rent" + carId);
                    RentDAO rentDAO = RentDAO.getInstance();
                    request.setAttribute("rent", rentDAO.selectByRenteeIdAndCarId(user.getId(), carID)); // is it me that renting?
                    request.setAttribute("rentee", rentDAO.selectRenteeByRenterIdAndCarIdAndStatus(user.getId(), carID, RentStatus.CURRENT)); // who is renting currently
                }
                request.setAttribute("car", car);
            } catch (Exception e) {
                page = goHome(request, carDAO);
            }
        }
        return page;
    }

    private String goHome(HttpServletRequest request, CarDAO carDAO) {
        String page;
        page = "home";
        request.setAttribute("cars", carDAO.selectAll(true, -1, -1, request, CarStatus.NOT_ANY));
        return page;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
