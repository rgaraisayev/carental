/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.user;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.dao.RentDAO;
import az.edu.ada.carental.entities.Car;
import az.edu.ada.carental.entities.RentHistory;
import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.RentStatus;
import az.edu.ada.carental.enums.SessionKeys;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 *
 * @author ramco
 */
public class RentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String carId = request.getParameter("car_id");
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        boolean success = false;
        if (action.equals("acceptRent")) {
            if (session.getAttribute(SessionKeys.IS_LOGGED) != null && session.getAttribute(SessionKeys.LOGGED_USER) != null) {
                User user = (User) session.getAttribute(SessionKeys.LOGGED_USER);
                if (user != null) {
                    String renteeId = request.getParameter("rentee_id");
                    try {
                        int carID = Integer.parseInt(carId);
                        int renteeID = Integer.parseInt(renteeId);
                        RentDAO rentDAO = RentDAO.getInstance();
                        CarDAO carDAO = CarDAO.getInstance();
                        RentHistory rent = new RentHistory();
                        rent.setCarId(carID);
                        rent.setRenteeId(renteeID);
                        rent.setRenterId(user.getId());
                        rent.setStatus(RentStatus.CURRENT);
                        rentDAO.updateStatus(rent);
                        carDAO.updateStatusById(carID, CarStatus.AT_RENT);
                        success = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (action.equals("cancelRent")) {
            if (session.getAttribute(SessionKeys.IS_LOGGED) != null && session.getAttribute(SessionKeys.LOGGED_USER) != null) {
                User user = (User) session.getAttribute(SessionKeys.LOGGED_USER);
                if (user != null) {
                    String renteeId = request.getParameter("rentee_id");
                    try {
                        int carID = Integer.parseInt(carId);
                        int renteeID = Integer.parseInt(renteeId);
                        RentDAO rentDAO = RentDAO.getInstance();
                        CarDAO carDAO = CarDAO.getInstance();
                        RentHistory rent = new RentHistory();
                        rent.setCarId(carID);
                        rent.setRenteeId(renteeID);
                        rent.setRenterId(user.getId());
                        rent.setStatus(RentStatus.CANCELED);
                        rentDAO.updateStatus(rent);
                        carDAO.updateStatusById(carID, CarStatus.AVAILABLE);
                        success = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (action.equals("finishRentProcess")) {
             if (session.getAttribute(SessionKeys.IS_LOGGED) != null && session.getAttribute(SessionKeys.LOGGED_USER) != null) {
                User user = (User) session.getAttribute(SessionKeys.LOGGED_USER);
                if (user != null) {
                    String renteeId = request.getParameter("rentee_id");
                    try {
                        int carID = Integer.parseInt(carId);
                        int renteeID = Integer.parseInt(renteeId);
                        RentDAO rentDAO = RentDAO.getInstance();
                        CarDAO carDAO = CarDAO.getInstance();
                        RentHistory rent = new RentHistory();
                        rent.setCarId(carID);
                        rent.setRenteeId(renteeID);
                        rent.setRenterId(user.getId());
                        rent.setStatus(RentStatus.FINISHED);
                        rentDAO.updateStatus(rent);
                        carDAO.updateStatusById(carID, CarStatus.AVAILABLE);
                        success = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (!carId.isEmpty()) {
            try {
                if (session.getAttribute(SessionKeys.IS_LOGGED) != null && session.getAttribute(SessionKeys.LOGGED_USER) != null) {
                    User user = (User) session.getAttribute(SessionKeys.LOGGED_USER);
                    if (user != null) {
                        int carID = Integer.parseInt(carId);
                        RentDAO rentDAO = RentDAO.getInstance();
                        CarDAO carDAO = CarDAO.getInstance();
                        int owner_id = carDAO.selectUserIdByCarId(carID);
                        if (owner_id != -1) {
                            RentHistory rent = new RentHistory();
                            rent.setCarId(carID);
                            rent.setRenteeId(user.getId());
                            rent.setRenterId(owner_id);
                            if (action.equals("rent")) {
                                rent.setStatus(RentStatus.REQUESTED_FOR_RENT);
                                RentHistory oldRent = rentDAO.selectByRenteeIdAndCarId(owner_id, carID);
                                if (oldRent != null) {
                                    rentDAO.updateStatus(rent);
                                } else {
                                    rentDAO.insert(rent);
                                }
                                carDAO.updateStatusById(carID, CarStatus.AT_RENT);
                            } else if (action.equals("cancel")) {
                                rent.setStatus(RentStatus.CANCELED);
                                rentDAO.updateStatus(rent);
                                carDAO.updateStatusById(carID, CarStatus.AVAILABLE);
                            }
                            success = true;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        JSONObject oNObject = new JSONObject();
        oNObject.put("success", success);
        response.getWriter().print(oNObject.toJSONString());
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
