/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.user;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.dao.RequestDAO;
import az.edu.ada.carental.entities.Car;
import az.edu.ada.carental.entities.Request;
import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.RentType;
import az.edu.ada.carental.enums.RequestStatus;
import az.edu.ada.carental.enums.SessionKeys;
import az.edu.ada.carental.enums.car.DifferentialType;
import az.edu.ada.carental.enums.car.FuelType;
import az.edu.ada.carental.enums.car.TransmissionType;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.tomcat.util.http.fileupload.IOUtils;

/**
 *
 * @author ramco
 */
@WebServlet("/upload")
@MultipartConfig
public class UploadImageServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static String UPLOAD_DIRECTORY = "home";
    private Random rnd;

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        UPLOAD_DIRECTORY = getServletContext().getInitParameter("profile-images");
        rnd = new Random();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        CarDAO carDAO = CarDAO.getInstance();
        User user = ((User) request.getSession().getAttribute(SessionKeys.LOGGED_USER));
        if (user != null) {
            String brand = request.getParameter("brand");
            if (!validate(brand) || brand.equals("Choose")) {
                request.setAttribute("brand", false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String model = request.getParameter("model");
            if (!validate(model) || brand.equals("Choose")) {
                request.setAttribute("model", false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String description = request.getParameter("description");
            if (!validate(description)) {
                request.setAttribute("description", false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
//            request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String currency = request.getParameter("currency");
            if (!validate(currency)) {
                request.setAttribute(currency, false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String color = request.getParameter("color");
            if (!validate(color)) {
                request.setAttribute("color", false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String horsePower = request.getParameter("horsePower");
            if (!validate(horsePower)) {
                request.setAttribute("horsePower", false);
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String year = request.getParameter("year");
            if (!validate(year)) {
                request.setAttribute("year", false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String engineVolume = request.getParameter("engineVolume");
            if (!validate(engineVolume)) {
                request.setAttribute("engineVolume", false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String fuelType = request.getParameter("fuelType");
            if (!validate(fuelType)) {
                request.setAttribute(fuelType, false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String transType = request.getParameter("transType");
            if (!validate(transType)) {
                request.setAttribute(transType, false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String diffType = request.getParameter("diffType");
            if (!validate(diffType)) {
                request.setAttribute(diffType, false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            String price = request.getParameter("price");
            if (!validate(price)) {
                request.setAttribute("price", false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }

            String rentType = request.getParameter("rentType");
            if (!validate(rentType)) {
                request.setAttribute(rentType, false);
                request.setAttribute("page", "rentyourcar");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            Car car = new Car();
            car.setColor(color.toUpperCase());
            car.setCurrency(currency);
            car.setDescription(description.trim());
            car.setDifferentialType(DifferentialType.valueOf(diffType.toUpperCase()));
            car.setEngineVolume(Integer.parseInt(engineVolume));
            car.setYear(Integer.parseInt(year));
            car.setFuelType(FuelType.valueOf(fuelType.toUpperCase()));
            car.setHorsePower(Integer.parseInt(horsePower));
            car.setPrice(Double.parseDouble(price));
            car.setTransmissionType(TransmissionType.valueOf(transType.toUpperCase()));
            car.setRentType(RentType.valueOf(rentType.toUpperCase()));
            car.setUser(user);
            car.setModelId(Integer.parseInt(model));

            Part mainImage = request.getPart("mainImage");
            Part image = request.getPart("image");
            Part image1 = request.getPart("image1");
            Part image2 = request.getPart("image2");
            Part image3 = request.getPart("image3");
            int i = 0;
            if (mainImage != null && mainImage.getSize() > 0) {
                try {
                    InputStream fileContent = mainImage.getInputStream();
                    String fileName = brand + "_" + year + "_" + i + "_" + getRandomInt();
                    String path = writeToDISC(fileContent, fileName);
                    car.setImageUrl(path);
                } catch (Exception e) {
                    ///
                }
            }
            ArrayList<String> urls = new ArrayList<String>();
            try {
                i++;
                InputStream fileContent;
                String fileName;
                String url;
                if (image != null && image.getSize() > 0) {
                    fileContent = image.getInputStream();
                    fileName = brand + "_" + year + "_" + i + "_" + getRandomInt();
                    url = writeToDISC(fileContent, fileName);
                    car.setImageUrl(url);
                    urls.add(url);
                }
                i++;
                if (image1 != null && image1.getSize() > 0) {
                    fileContent = image1.getInputStream();
                    fileName = brand + "_" + year + "_" + i + "_" + getRandomInt();
                    url = writeToDISC(fileContent, fileName);
                    car.setImageUrl(url);
                    urls.add(url);
                }
                i++;
                if (image2 != null && image2.getSize() > 0) {
                    fileContent = image2.getInputStream();
                    fileName = brand + "_" + year + "_" + i + "_" + getRandomInt();
                    url = writeToDISC(fileContent, fileName);
                    car.setImageUrl(url);
                    urls.add(url);
                }
                i++;
                if (image3 != null && image3.getSize() > 0) {
                    fileContent = image3.getInputStream();
                    fileName = brand + "_" + year + "_" + i + "_" + getRandomInt();
                    url = writeToDISC(fileContent, fileName);
                    car.setImageUrl(url);
                    urls.add(url);
                }
                i++;
            } catch (Exception e) {
                ///
            }

            car.setImageUrls(urls);
            car.setStatus(CarStatus.NOT_AVAILABLE);
            car.setUserId(user.getId());
            String uuid = carDAO.insert(car);
            if (uuid != null && !uuid.isEmpty()) {
                car.setUuid(uuid);
                RequestDAO requestDAO = RequestDAO.getInstance();
                Request r = new Request();
                java.sql.Date sqlDate = new java.sql.Date((new Date()).getTime());
                System.out.println(sqlDate.getTime());
                r.setDate(sqlDate);
                r.setUser(user);
                r.setCar(car);
                r.setStatus(RequestStatus.PENDING);
                requestDAO.insert(r);
                request.setAttribute("page", "profile");
                request.getRequestDispatcher("user/index.jsp").forward(request, response);
                return;
            }
            request.setAttribute("page", "rentyourcar");
            request.getRequestDispatcher("user/index.jsp").forward(request, response);

        } else {
            request.setAttribute("page", "login");
            request.getRequestDispatcher("user/index.jsp").forward(request, response);
        }

    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    public String writeToDISC(InputStream fileContent, String name) throws IOException {
        String path = getServletContext().getRealPath("/") + "carImages/";
        String fakePath = getServletContext().getContextPath() + "/carImages/" + name + ".jpg";
        System.out.println(fakePath);
        File newF = new File(path);
        System.out.println(newF.mkdir());
        File outputFile = new File(newF, name + ".jpg");
        String formatName = "jpg";
        BufferedImage bufferedImage = ImageIO.read(fileContent);
        ImageIO.write(bufferedImage, formatName, outputFile);
        return fakePath;
    }

    public int getRandomInt() {
        return rnd.nextInt(10000000);
    }

    private boolean validate(String v) {
        if (v != null && !v.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private void go(HttpServletRequest request, String name) {
        request.setAttribute(name, false);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
