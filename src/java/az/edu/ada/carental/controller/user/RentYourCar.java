/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.controller.user;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.entities.CarModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author ramco
 */
public class RentYourCar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RentYourCar</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RentYourCar at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("name");
        String where = request.getParameter("where");
        List<CarModel> m = CarDAO.getInstance().getAllModelsByBrandName(name);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        object.put("id", -1);
        object.put("name", "Choose");
        array.add(object);
        if (!m.isEmpty()) {
            for (CarModel model : m) {
                object = new JSONObject();
                object.put("id", model.getId());
                object.put("name", model.getName());
                array.add(object);
            }
        }
        PrintWriter out = response.getWriter();
        out.print(array.toJSONString());
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String where = request.getParameter("where");
        List<CarModel> m = CarDAO.getInstance().getAllModelsByBrandName(name);
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        object.put("id", -1);
        object.put("name", "Choose");
        array.add(object);
        if (!m.isEmpty()) {
            for (CarModel model : m) {
                object = new JSONObject();
                object.put("id", model.getId());
                object.put("name", model.getName());
                array.add(object);
            }
        }
        PrintWriter out = response.getWriter();
        out.print(array.toJSONString());
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
