/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.listeners;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class MHTTPSessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        Long guests = (Long) session.getServletContext().getAttribute("guests");
        if (guests != null) {
            guests++;
            session.getServletContext().setAttribute("guests", guests);
        } else {
            session.getServletContext().setAttribute("guests", (long) 1);
        }
        System.out.println("Session Created");

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        Long guests = (Long) session.getServletContext().getAttribute("guests");
        if (guests != null) {
            guests--;
            session.getServletContext().setAttribute("guests", guests);
        } else {
            session.getServletContext().setAttribute("guests", (long) 0);
        }
        System.out.println("Session Destroyed");
    }

}
