/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.dao.abstractDatabase;

import java.sql.Connection;
import java.sql.DriverManager; 
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public abstract class DatabaseConnector{

    protected static final String DB_SCHEMA = "ada_carental_db";
    protected static final String DB_URL = "jdbc:mysql://localhost:3306/" + DB_SCHEMA+"?useUnicode=yes&characterEncoding=UTF-8";
    protected static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    protected static final String DB_USER = "root";
    protected static final String DB_PASSWORD = "root";

    public DatabaseConnector() {
        try {
            Class.forName(JDBC_DRIVER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Connection getConnection() {
        try {
            return DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnector.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
