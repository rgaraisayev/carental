/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.dao.abstractDatabase;

import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONObject;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public interface IDatabaseCRUD<T> {

    public boolean insert(T data);

    public boolean delete(T data);

    public boolean update(T data);

    public List<T> selectAll();

    public T selectById(T data, HttpServletRequest re);
    
    public JSONObject advancedSearch(AbstractSearchEngine data);
}
