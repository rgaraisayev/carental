/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.dao;

import az.edu.ada.carental.dao.abstractDatabase.DatabaseConnector;
import az.edu.ada.carental.dao.abstractDatabase.IDatabaseCRUD;
import az.edu.ada.carental.entities.Car;
import az.edu.ada.carental.entities.RentHistory;
import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.RentStatus;
import az.edu.ada.carental.enums.RentType;
import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class RentDAO extends DatabaseConnector {

    protected ResultSet resultSet;
    protected PreparedStatement preparedStatement;
    protected Connection connection;

    protected static final String DB_RENT_TABLE = "RENT_HISTORY";
    /* 9 rows*/
    public static final String C[] = {"car_id", "renter_id", "status", "rentee_id"};
    private static RentDAO rentDAO;

    public static RentDAO getInstance() {
        return rentDAO != null ? rentDAO : (rentDAO = new RentDAO());
    }

    public boolean insert(RentHistory rentHistory) {
        try {
            String query = "insert into " + DB_RENT_TABLE + "(" + C[0] + "," + C[1] + "," + C[2] + "," + C[3] + ") "
                    + "values(?,?,?,?)";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, rentHistory.getCarId());
            preparedStatement.setLong(2, rentHistory.getRenterId());
            preparedStatement.setString(3, rentHistory.getStatus().toString());
            preparedStatement.setLong(4, rentHistory.getRenteeId());
            preparedStatement.execute();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(RentDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void updateStatus(RentHistory rentHistory) {
        try {
            String query = "update " + DB_RENT_TABLE + " set status=? where car_id=? and renter_id=? and rentee_id=?";
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setInt(2, rentHistory.getCarId());
            preparedStatement.setInt(3, rentHistory.getRenterId());
            preparedStatement.setString(1, rentHistory.getStatus().toString());
            preparedStatement.setInt(4, rentHistory.getRenteeId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<RentHistory> selectAll(boolean withRelation, HttpServletRequest request) {
        try {
            String query = "select * from " + DB_RENT_TABLE;
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            List<RentHistory> rents = new ArrayList<RentHistory>();
            while (resultSet.next()) {
                RentHistory rent = getRentHistory();
                if (withRelation) {
                    rent = selectRentRelations(rents, rent, request);
                }
                rents.add(rent);
            }
            return rents;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    private RentHistory selectRentRelations(List<RentHistory> rents, RentHistory rent, HttpServletRequest request) {
        boolean haveRentee = false;
        boolean haveRenter = false;
        boolean haveCar = false;
        for (RentHistory rentHistory : rents) {
            if (rentHistory.getCar().getId() == rent.getCarId()) {
                haveCar = true;
                rent.setCar(rentHistory.getCar());
            }
            if (rentHistory.getRentee().getId() == rent.getRenteeId()) {
                haveRentee = true;
                rent.setRentee(rentHistory.getRentee());
            }
            if (rentHistory.getRenter().getId() == rent.getRenterId()) {
                haveRenter = true;
                rent.setRenter(rentHistory.getRenter());
            }
        }
        if (!haveCar) {
            rent.setCar(CarDAO.getInstance().selectById(rent.getCarId(), false, request, CarStatus.ANY));
        }
        if (!haveRentee) {
            rent.setRentee(UserDAO.getInstance().selectById(rent.getRenteeId()));
        }
        if (!haveRenter) {
            rent.setRenter(UserDAO.getInstance().selectById(rent.getRenterId()));
        }
        return rent;
    }

    private RentHistory getRentHistory() throws SQLException {
        RentHistory rent = new RentHistory();
        rent.setCarId(resultSet.getInt(C[0]));
        rent.setRenterId(resultSet.getInt(C[1]));
        rent.setStatus(RentStatus.valueOf(resultSet.getString(C[2])));
        rent.setRenteeId(resultSet.getInt(C[3]));
        return rent;
    }

    public ArrayList<RentHistory> selectByCarId(int carId, boolean withRelation, HttpServletRequest request) {
        try {
            String query = "select * from " + DB_RENT_TABLE + " where car_id=?";
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setInt(1, carId);
            resultSet = preparedStatement.executeQuery();
            ArrayList<RentHistory> rents = new ArrayList<RentHistory>();
            while (resultSet.next()) {
                RentHistory rent = getRentHistory();
                if (withRelation) {
                    rent = selectRentRelations(rents, rent, request);
                }
                rents.add(rent);
            }
            return rents;
        } catch (Exception e) {
            return null;
        } finally {
            closeAll();
        }

    }

    public ArrayList<RentHistory> selectByCarIdAndStatus(int carId, CarStatus status, boolean withRelation, HttpServletRequest request) {
        try {
            String query = "select * from " + DB_RENT_TABLE + " where car_id=? and status=?";
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setInt(1, carId);
            preparedStatement.setString(2, status.toString());
            resultSet = preparedStatement.executeQuery();
            ArrayList<RentHistory> rents = new ArrayList<RentHistory>();
            while (resultSet.next()) {
                RentHistory rent = getRentHistory();
                if (withRelation) {
                    rent = selectRentRelations(rents, rent, request);
                }
                rents.add(rent);
            }
            return rents;
        } catch (Exception e) {
            return null;
        } finally {
            closeAll();
        }

    }

    public RentHistory selectByCarIdRenteeIdAndStatus(int carId, int renteeId, RentStatus status, boolean withRelation) {
        try {
            String query;
            query = "select * from " + DB_RENT_TABLE + " where car_id=? and rentee_id=? and status=?";
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setInt(1, carId);
            preparedStatement.setLong(2, renteeId);
            preparedStatement.setString(3, status.toString());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                RentHistory rent = getRentHistory();
                return rent;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;

    }

    public ArrayList<RentHistory> selectByRenterIdAndStatus(int renterId, RentStatus status, int limit, HttpServletRequest request) {
        try {
            String query;
            if (limit > 0) {
//                query = "select r.car_id, r.rentee_id, c.brand, c.model, c.imageUrl  from " + DB_RENT_TABLE + " r  join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id) where r.renter_id=? and r.status=? limit " + limit;
                query = "select * from " + DB_RENT_TABLE + " where renter_id=? and status=? limit " + limit;
            } else {
//                query = "select r.car_id, r.rentee_id, c.brand, c.model, c.imageUrl from " + DB_RENT_TABLE + " r  join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id) where r.renter_id=? and r.status=?";
                query = "select * from " + DB_RENT_TABLE + " where renter_id=? and status=?";
            }
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setLong(1, renterId);
            preparedStatement.setString(2, status.toString());
            resultSet = preparedStatement.executeQuery();
            ArrayList<RentHistory> rents = new ArrayList<RentHistory>();
            while (resultSet.next()) {
                RentHistory rent = getRentHistory();
                rent.setCar(CarDAO.getInstance().selectById(rent.getCarId(), true, request, CarStatus.ANY));
                rents.add(rent);
            }
            return rents;
        } catch (Exception e) {
            return null;
        } finally {
            closeAll();
        }
    }

    public ArrayList<RentHistory> selectByRenteeIdAndStatus(int renteeId, RentStatus status, int limit, HttpServletRequest request) {
        try {
            String query;
            if (limit > 0) {
//                query = "select r.car_id, r.renter_id, c.brand, c.model, c.imageUrl  from " + DB_RENT_TABLE + " r  join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id) where r.rentee_id=? and r.status=? limit " + limit;
                query = "select * from " + DB_RENT_TABLE + " where rentee_id=? and status=? limit " + limit;
            } else {
//                query = "select r.car_id, r.renter_id, c.brand, c.model, c.imageUrl from " + DB_RENT_TABLE + " r  join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id) where r.rentee_id=? and r.status=?";
                query = "select * from " + DB_RENT_TABLE + " where rentee_id=? and status=?";
            }
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setLong(1, renteeId);
            preparedStatement.setString(2, status.toString());
            resultSet = preparedStatement.executeQuery();
            ArrayList<RentHistory> rents = new ArrayList<RentHistory>();
            while (resultSet.next()) {
                RentHistory rent = getRentHistory();
                rent.setCar(CarDAO.getInstance().selectById(rent.getCarId(), true, request, CarStatus.ANY));
                rents.add(rent);
            }
            return rents;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public ArrayList<RentHistory> selectCarsByRenteeId(int renteeId, int limit, HttpServletRequest request) {
        try {
            String query;
            if (limit > 0) {
//                query = "select r.car_id, r.renter_id, r.status, c.brand, c.model, c.imageUrl  from " + DB_RENT_TABLE + " r  join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id) where r.rentee_id=? limit " + limit;
                query = "select * from " + DB_RENT_TABLE + " where rentee_id=? and status=? limit " + limit;
            } else {
//                query = "select r.car_id, r.renter_id, r.status, c.brand, c.model, c.imageUrl from " + DB_RENT_TABLE + " r  join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id) where r.rentee_id=? ";
                query = "select * from " + DB_RENT_TABLE + " where rentee_id=? and status=?";
            }
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setLong(1, renteeId);
            resultSet = preparedStatement.executeQuery();
            ArrayList<RentHistory> rents = new ArrayList<RentHistory>();
            while (resultSet.next()) {
                RentHistory rent = getRentHistory();
                rent.setCar(CarDAO.getInstance().selectById(rent.getCarId(), true, request, CarStatus.ANY));
                rents.add(rent);
            }
            return rents;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public ArrayList<RentHistory> selectRentHistoryByRenteeId(int renteeId, int limit, HttpServletRequest request) {
        try {
            String query;
            if (limit > 0) {
//                query = "select r.car_id, r.renter_id, r.status, c.brand, c.model, c.imageUrl  from " + DB_RENT_TABLE + " r  join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id) where r.rentee_id=? and r.status!=0 limit " + limit;
                query = "select * from " + DB_RENT_TABLE + " where rentee_id=? limit " + limit;
            } else {
//                query = "select r.car_id, r.renter_id, r.status, c.brand, c.model, c.imageUrl from " + DB_RENT_TABLE + " r  join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id) where r.rentee_id=? and r.status=!0";
                query = "select * from " + DB_RENT_TABLE + " where rentee_id=?";
            }
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setLong(1, renteeId);
            resultSet = preparedStatement.executeQuery();
            ArrayList<RentHistory> rents = new ArrayList<RentHistory>();
            while (resultSet.next()) {
                RentHistory rent = getRentHistory();
                rent.setCar(CarDAO.getInstance().selectById(rent.getCarId(), true, request, CarStatus.ANY));
                rents.add(rent);
            }
            return rents;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public ArrayList<RentHistory> selectByRenterId(int renterId) {
        try {
            String query = "select r.car_id, r.status, r.rentee_id, c.brand, c.model, c.imageUrl from " + DB_RENT_TABLE + " r join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id)  where r.renter_id=?";
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setInt(1, renterId);
            resultSet = preparedStatement.executeQuery();
            ArrayList<RentHistory> rents = new ArrayList<RentHistory>();
            while (resultSet.next()) {
                RentHistory rent = new RentHistory();
                rent.setRenterId(renterId);
                rent.setCarId(resultSet.getInt(0));
                rent.setRenteeId(resultSet.getInt(2));
                rent.setCarBrand(resultSet.getString(3));
                rent.setCarModel(resultSet.getString(4));
                rent.setCarImageUrl(resultSet.getString(5));
                rent.setStatus(RentStatus.valueOf(resultSet.getString("r.status")));
                rents.add(rent);
            }
            return rents;
        } catch (Exception e) {
            return null;
        } finally {
            closeAll();
        }
    }

    public ArrayList<RentHistory> selectByRenteeId(int renteeId, int limit) {
        try {
            String query;
            if (limit > 0) {
                query = "select r.car_id, r.status, r.renter_id, c.brand, c.model, c.imageUrl from " + DB_RENT_TABLE + " r join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id)  where r.rentee_id=? limit " + limit;
            } else {
                query = "select r.car_id, r.status, r.renter_id, c.brand, c.model,  c.imageUrl from " + DB_RENT_TABLE + " r join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id=c.id)  where r.rentee_id=?";
            }
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setLong(1, renteeId);
            resultSet = preparedStatement.executeQuery();
            ArrayList<RentHistory> rents = new ArrayList<RentHistory>();
            while (resultSet.next()) {
                RentHistory rent = new RentHistory();
                rent.setRenteeId(renteeId);
                rent.setCarId(resultSet.getInt(1));
                rent.setRenterId(resultSet.getInt(3));
                rent.setCarBrand(resultSet.getString(4));
                rent.setCarModel(resultSet.getString(5));
                rent.setCarImageUrl(resultSet.getString(6));
                rent.setStatus(RentStatus.valueOf(resultSet.getString("r.status")));
                rents.add(rent);
            }
            return rents;
        } catch (Exception e) {
            return null;
        } finally {
            closeAll();
        }
    }

    public RentHistory selectByRenteeIdAndCarId(int renteeId, int carId) {
        try {
            String query;
            query = "select * from " + DB_RENT_TABLE + " where rentee_id=?  and car_id=?";
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setLong(1, renteeId);
            preparedStatement.setInt(2, carId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                RentHistory rent = getRentHistory();
//                rent.setRentee(UserDAO.getInstance().selectById(renteeId));
                return rent;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }

    public RentHistory selectByRenterIdAndCarId(int renterId, int carId, int limit) {
        try {
            String query;
            query = "select r.car_id, r.status, r.rentee_id, c.brand, c.model, c.imageUrl from " + DB_RENT_TABLE + " r join " + CarDAO.DB_CAR_TABLE + " c on (r.car_id = c.id)  where r.renter_id=? and r.car_id=? ";
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setLong(1, renterId);
            preparedStatement.setInt(2, carId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                RentHistory rent = new RentHistory();
                rent.setRenterId(renterId);
                rent.setCarId(resultSet.getInt("r.car_id"));
                rent.setRenteeId(resultSet.getInt("r.rentee_id"));
                rent.setCarBrand(resultSet.getString("c.brand"));
                rent.setCarModel(resultSet.getString("c.model"));
                rent.setCarImageUrl(resultSet.getString("c.imageUrl"));
                rent.setStatus(RentStatus.valueOf(resultSet.getString("r.status")));
                return rent;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }

    public User selectRenteeByRenterIdAndCarIdAndStatus(long renterId, int carId, RentStatus status) {
        try {
            String query;
            query = "select u.id, u.name, u.surname, u.email, u.phone from " + DB_RENT_TABLE + " r join " + UserDAO.DB_USER_TABLE + " u on (r.rentee_id = u.id)  where r.renter_id=? and r.car_id=? and r.status=?";
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setLong(1, renterId);
            preparedStatement.setInt(2, carId);
            preparedStatement.setString(3, status.toString());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt(1));
                user.setName(resultSet.getString(2));
                user.setSurname(resultSet.getString(3));
                user.setEmail(resultSet.getString(4));
                user.setPhone(resultSet.getString(5));
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }

    public boolean isCarAvailable(int renterId, int carId) {
        try {
            String query = "select status from " + DB_RENT_TABLE + " where renter_id=? and car_id=?";
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setInt(1, renterId);
            preparedStatement.setInt(2, carId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString(1).equals(RentStatus.FINISHED);

            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        } finally {
            closeAll();
        }
    }

    public boolean isCarAvailable(int carId) {
        try {
            String query = "select status from " + DB_RENT_TABLE + " where car_id=?";
            System.out.println(query);
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setInt(1, carId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(0) == 0;

            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        } finally {
            closeAll();
        }
    }

    public RentHistory selectById(RentHistory data) {
        return null;
    }

    public JSONObject advancedSearch(AbstractSearchEngine data) {
        JSONObject result = new JSONObject();
        JSONArray array = new JSONArray();
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(data.getQuery());
            resultSet = preparedStatement.executeQuery();
            JSONArray ja = new JSONArray();
            while (resultSet.next()) {
                ja.add(resultSet.getString(1));
                ja.add(resultSet.getString(2));
                ja.add(resultSet.getString(3));
                ja.add(resultSet.getString(4));
                ja.add(resultSet.getString(5));
                array.add(ja);
            }

            result.put("iTotalRecords", data.getTotalDataCount());
            result.put("iTotalDisplayRecords", ja.size());
            result.put("aaData", array);
            return result;
        } catch (Exception e) {
            return null;
        } finally {
            closeAll();
        }
    }

    public boolean delete(RentHistory data) {
        try {
            String query = "update " + DB_RENT_TABLE + " set status=0 where car_id=? and rentee_id=? and renter_id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, data.getCarId());
            preparedStatement.setLong(2, data.getRenteeId());
            preparedStatement.setLong(3, data.getRenterId());
            preparedStatement.execute();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean update(RentHistory data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void closeAll() {
        try {
            if (connection != null) {
                connection.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(RentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
