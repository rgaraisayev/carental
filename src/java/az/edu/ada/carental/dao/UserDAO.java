/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.dao;

import az.edu.ada.carental.dao.abstractDatabase.DatabaseConnector;
import az.edu.ada.carental.dao.abstractDatabase.IDatabaseCRUD;
import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.UserStatus;
import az.edu.ada.carental.enums.UserType;
import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class UserDAO extends DatabaseConnector {

    protected ResultSet resultSet;
    protected PreparedStatement preparedStatement;
    protected Connection connection;

    protected static final String DB_USER_TABLE = "USERS";
    protected static final String DB_BLACK_LIST_TABLE = "BLACK_LIST";
    /* 8 rows*/
    public static final String C[] = {"ID", "NAME", "SURNAME", "EMAIL", "PASSWORD", "PHONE", "PROFILEIMAGEURL", "TYPE"};

    private static UserDAO userDAO;

    public static UserDAO getInstance() {
        return userDAO != null ? userDAO : (new UserDAO());
    }

    public boolean insert(User user) {
        try {
            String query = "insert into " + DB_USER_TABLE + "(" + C[1] + "," + C[2] + "," + C[3] + "," + C[4] + "," + C[5] + "," + C[6] + "," + C[7] + ") "
                    + "values(?,?,?,?,?,?,?)";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhone());
            preparedStatement.setString(6, user.getProfileImageUrl());
            preparedStatement.setString(7, user.getType().toString());
            preparedStatement.execute();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean delete(User data) {
        try {
            String query = "delete from " + DB_USER_TABLE + " where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, data.getId());
            preparedStatement.execute();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean update(User user) {
        try {
            String query = "update " + DB_USER_TABLE + " set " + C[1] + "=?," + C[2] + "=?," + C[3] + "=?," + C[4] + "=?," + C[5] + "=?," + C[6] + "=?," + C[7] + "=? where " + C[0] + "=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhone());
            preparedStatement.setString(6, user.getProfileImageUrl());
            preparedStatement = setUserType(user.getType(), 7);
            preparedStatement.setLong(8, user.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
 public boolean updatePartially(User user) {
        try {
            String query = "update " + DB_USER_TABLE + " set " + C[1] + "=?," + C[2] + "=?," + C[5] + "=? where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getPhone());
            preparedStatement.setLong(4, user.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public List<User> selectAll() {
        try {
            String query = "select * from " + DB_USER_TABLE;
            executeQuery(query);
            List<User> users = new ArrayList<User>();
            while (resultSet.next()) {
                users.add(getUser());
            }
            return users;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

//    @Override
    public User selectById(int id) {
        try {
            String query = "select * from " + DB_USER_TABLE + " where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return getUser();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }

    public User getUserByUsernameAndPassword(String email, String password) {
        try {
            String query = "select * from " + DB_USER_TABLE + " where email=? and password=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return getUser();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }

    public boolean isUserExistByEmail(String email) {
        try {
            String query = "select id from " + DB_USER_TABLE + " where email=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return false;
    }

    public long selectUserCount() {
        try {
            String query = "select count(id) as c from " + DB_USER_TABLE;
            executeQuery(query);
            if (resultSet.next()) {
                return resultSet.getLong("c");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return 0;
    }

    public JSONObject advancedSearch(AbstractSearchEngine data) {
        JSONObject result = new JSONObject();
        JSONArray array = new JSONArray();
        try {
            executeQuery(data.getQuery());
            JSONArray ja = new JSONArray();
            while (resultSet.next()) {
                ja.add(resultSet.getString(1));
                ja.add(resultSet.getString(2));
                ja.add(resultSet.getString(3));
                ja.add(resultSet.getString(4));
                ja.add(resultSet.getString(5));
                array.add(ja);
            }

            result.put("iTotalRecords", data.getTotalDataCount());
            result.put("iTotalDisplayRecords", ja.size());
            result.put("aaData", array);
            return result;
        } catch (Exception e) {
            return null;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
                resultSet.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private PreparedStatement setUserType(UserType type, int index) throws SQLException {
        if (type.equals(UserType.ADMIN)) {
            preparedStatement.setString(index, "ADMIN");
        } else {
            preparedStatement.setString(index, "USER");
        }
        return preparedStatement;
    }

    private User getUser() throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt(C[0]));
        user.setName(resultSet.getString(C[1]));
        user.setSurname(resultSet.getString(C[2]));
        user.setEmail(resultSet.getString(C[3]));
        user.setPassword(resultSet.getString(C[4]));
        user.setPhone(resultSet.getString(C[5]));
        user.setProfileImageUrl(resultSet.getString(C[6]));
        if (resultSet.getString(C[7]).equals("ADMIN")) {
            user.setType(UserType.ADMIN);
        } else {
            user.setType(UserType.USER);
        }
        return user;
    }

    private void executeQuery(String query) throws SQLException {
        connection = getConnection();
        preparedStatement = connection.prepareStatement(query);
        resultSet = preparedStatement.executeQuery();
    }

    private void closeAll() {
        try {
            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
