/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.dao;

import az.edu.ada.carental.dao.abstractDatabase.DatabaseConnector;
import az.edu.ada.carental.dao.abstractDatabase.IDatabaseCRUD;
import az.edu.ada.carental.entities.Car;
import az.edu.ada.carental.entities.CarBrand;
import az.edu.ada.carental.entities.CarModel;
import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.RentType;
import az.edu.ada.carental.enums.car.DifferentialType;
import az.edu.ada.carental.enums.car.FuelType;
import az.edu.ada.carental.enums.car.TransmissionType;
import az.edu.ada.carental.utility.search.domen.CarSearch;
import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class CarDAO extends DatabaseConnector {

    protected ResultSet resultSet;
    protected PreparedStatement preparedStatement;
    protected Connection connection;

    public static final String DB_CAR_TABLE = "CARS";
    public static final String DB_BRAND_TABLE = "CAR_BRANDS";
    public static final String DB_MODEL_TABLE = "CAR_MODELS";
    protected static final String DB_IMAGE_TABLE = "IMAGES";
    /* 9 rows*/
    public static final String C[] = {"id", "model_id", "color", "description", "imageurl", "horse_power", "status",
        "year", "engine_volume", "fuel_type", "transmission_type", "differential_type",
        "user_id", "price", "rent_type", "currency", "uuid"};
    public static final String BRAND[] = {"id", "name"};
    public static final String MODEL[] = {"id", "name", "brand_id"};
    public static final String IMAGES[] = {"id", "url", "car_id"};
    private static CarDAO carDAO;

    public static CarDAO getInstance() {
        return carDAO != null ? carDAO : (carDAO = new CarDAO());
    }

//    @Override
    public String insert(Car car) {
        try {
            String uuid = String.valueOf(UUID.randomUUID());
            String query = "insert into " + DB_CAR_TABLE + "(" + C[1] + "," + C[2] + "," + C[3] + "," + C[4] + "," + C[5] + ","
                    + C[6] + "," + C[7] + "," + C[8] + "," + C[9] + ", " + C[10] + "," + C[11] + ","
                    + C[12] + "," + C[13] + "," + C[14] + "," + C[15] + "," + C[16] + ") "
                    + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, car.getModelId());
            preparedStatement.setString(2, car.getColor());
            preparedStatement.setString(3, car.getDescription());
            preparedStatement.setString(4, car.getImageUrl());
            preparedStatement.setInt(5, car.getHorsePower());
            preparedStatement.setString(6, car.getStatus().toString());
            preparedStatement.setInt(7, car.getYear());
            preparedStatement.setInt(8, car.getEngineVolume());
            preparedStatement.setString(9, car.getFuelType().toString());
            preparedStatement.setString(10, car.getTransmissionType().toString());
            preparedStatement.setString(11, car.getDifferentialType().toString());
            preparedStatement.setInt(12, car.getUserId());
            preparedStatement.setDouble(13, car.getPrice());
            preparedStatement.setString(14, car.getRentType().toString());
            preparedStatement.setString(15, car.getCurrency());
            preparedStatement.setString(16, uuid);
            preparedStatement.execute();
            insertImages(car.getImageUrls(), uuid);
            return uuid;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean insertImages(ArrayList<String> urls, String uuid) throws SQLException {
        if (urls != null && !urls.isEmpty()) {
            for (String url : urls) {
                String query = "insert into " + DB_IMAGE_TABLE + " (url, car_uuid) values(?, ?)";
                preparedStatement = getConnection().prepareStatement(query);
                preparedStatement.setString(1, url);
                preparedStatement.setString(2, uuid);
                preparedStatement.execute();
            }
        }
        return true;
    }

    public boolean delete(Car data) {
        try {
            String query = "update " + DB_CAR_TABLE + " set status=0 where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, data.getId());
            preparedStatement.execute();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean update(Car car) {
        try {
            String query = "update " + DB_CAR_TABLE + " set " + C[1] + "=?," + C[2] + "=?," + C[3] + "=?," + C[4] + "=?," + C[5] + "=?," + C[6] + "=?,"
                    + C[7] + "=?, " + C[8] + "=?, " + C[9] + "=?, " + C[10] + C[11] + "=?, "
                    + C[12] + "=?, " + C[13] + "=?, " + C[14] + "=?, " + C[15] + "=? where " + C[0] + "=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, car.getId());
            preparedStatement.setInt(1, car.getModelId());
            preparedStatement.setString(2, car.getColor());
            preparedStatement.setString(3, car.getDescription());
            preparedStatement.setString(4, car.getImageUrl());
            preparedStatement.setInt(5, car.getHorsePower());
            preparedStatement.setString(6, car.getStatus().toString());
            preparedStatement.setInt(7, car.getYear());
            preparedStatement.setInt(8, car.getEngineVolume());
            preparedStatement.setString(9, car.getFuelType().toString());
            preparedStatement.setString(10, car.getTransmissionType().toString());
            preparedStatement.setString(11, car.getDifferentialType().toString());
            preparedStatement.setInt(12, car.getUserId());
            preparedStatement.setDouble(13, car.getPrice());
            preparedStatement.setString(14, car.getRentType().toString());
            preparedStatement.setString(15, car.getCurrency());
            preparedStatement.executeUpdate();
            updateImages(car.getImageUrls(), car.getId());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean updateStatusById(long id, CarStatus carStatus) {
        try {
            String query = "update " + DB_CAR_TABLE + " set status=? where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(2, id);
            preparedStatement.setString(1, carStatus.toString());
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void updateImages(ArrayList<String> urls, long carId) {
        try {
            for (String url : urls) {
                String query = "update " + DB_IMAGE_TABLE + " set url=? where car_id=?";
                preparedStatement = getConnection().prepareStatement(query);
                preparedStatement.setString(1, url);
                preparedStatement.setLong(2, carId);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException ex) {
            Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Car> selectAll(boolean withRelation, int limit, int offset, HttpServletRequest request, CarStatus carStatus) {
        try {
            String query;
            if (limit >= 0) {
                if (carStatus.equals(CarStatus.ANY)) {
                    query = "select c.id, c.status, c.imageUrl, b.name, m.name, c.description, c.price, c.currency from " + CarDAO.DB_CAR_TABLE + " c join " + CarDAO.DB_MODEL_TABLE + " m on(c.model_id=m.id) join " + CarDAO.DB_BRAND_TABLE + " b on(m.brand_id=b.id) limit " + limit;
//                    query = "select * from " + DB_CAR_TABLE + " limit " + limit;
                } else {
                    query = "select c.id, c.status, c.imageUrl, b.name, m.name, c.description, c.price, c.currency from " + CarDAO.DB_CAR_TABLE + " c join " + CarDAO.DB_MODEL_TABLE + " m on(c.model_id=m.id) join " + CarDAO.DB_BRAND_TABLE + " b on(m.brand_id=b.id)  where status!='NOT_AVAILABLE' limit " + limit;
//                    query = "select * from " + DB_CAR_TABLE + " where status!='NOT_AVAILABLE' limit " + limit;
                }
                if (offset > 0) {
                    if (carStatus.equals(CarStatus.ANY)) {
                        query = "select c.id, c.status, c.imageUrl, b.name, m.name, c.description, c.price, c.currency from " + CarDAO.DB_CAR_TABLE + " c join " + CarDAO.DB_MODEL_TABLE + " m on(c.model_id=m.id) join " + CarDAO.DB_BRAND_TABLE + " b on(m.brand_id=b.id)   limit " + limit + " " + offset;
//                        query = "select * from " + DB_CAR_TABLE + "  limit " + limit + " " + offset;
                    } else {
                        query = "select c.id, c.status, c.imageUrl, b.name, m.name, c.description, c.price, c.currency from " + CarDAO.DB_CAR_TABLE + " c join " + CarDAO.DB_MODEL_TABLE + " m on(c.model_id=m.id) join " + CarDAO.DB_BRAND_TABLE + " b on(m.brand_id=b.id) where status!='NOT_AVAILABLE'  limit " + limit + " " + offset;
//                        query = "select * from " + DB_CAR_TABLE + " where status!='NOT_AVAILABLE' limit " + limit + " " + offset;

                    }
                }
            } else if (carStatus.equals(CarStatus.ANY)) {
                query = "select c.id, c.status, c.imageUrl, b.name, m.name, c.description, c.price, c.currency from " + CarDAO.DB_CAR_TABLE + " c join " + CarDAO.DB_MODEL_TABLE + " m on(c.model_id=m.id) join " + CarDAO.DB_BRAND_TABLE + " b on(m.brand_id=b.id)";
//                query = "select * from " + DB_CAR_TABLE;
            } else {
                query = "select c.id, c.status, c.imageUrl, b.name, m.name, c.description, c.price, c.currency from " + CarDAO.DB_CAR_TABLE + " c join " + CarDAO.DB_MODEL_TABLE + " m on(c.model_id=m.id) join " + CarDAO.DB_BRAND_TABLE + " b on(m.brand_id=b.id) where status!='NOT_AVAILABLE'";
//                query = "select * from " + DB_CAR_TABLE + " where status!='NOT_AVAILABLE'";
            }
            System.out.println(query);
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            List<Car> cars = new ArrayList<Car>();
            while (resultSet.next()) {
                Car car = new Car();
                car.setId(resultSet.getInt(1));
                car.setStatus(CarStatus.valueOf(resultSet.getString(2)));
                car.setImageUrl(resultSet.getString(3));
                CarBrand carBrand = new CarBrand();
                carBrand.setName(resultSet.getString(4));
                CarModel carModel = new CarModel();
                carModel.setBrand(carBrand);
                carModel.setName(resultSet.getString(5));
                car.setModel(carModel);
                car.setDescription(resultSet.getString(6));
                car.setPrice(resultSet.getDouble(7));
                car.setCurrency(resultSet.getString(8));
                cars.add(car);
            }
            return cars;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            System.out.println("finnaly");
            closeAll();
        }
    }

    public List<CarBrand> getAllBrands() {
        try {
            String query = "select * from " + DB_BRAND_TABLE;
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            List<CarBrand> brands = new ArrayList<CarBrand>();
            while (resultSet.next()) {
                CarBrand brand = new CarBrand();
                brand.setId(resultSet.getInt(1));
                brand.setName(resultSet.getString(2));
                brands.add(brand);
            }
            return brands;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public List<Integer> getAllBrandIds(String name, String brand, String model) {
        try {
            connection = getConnection();
            String query;
            if (!brand.isEmpty() && model.isEmpty()) {
                query = "select id from " + DB_BRAND_TABLE + " b join " + DB_MODEL_TABLE + " m on (b.id=m.brand_id) where b.name LIKE ? and b.name=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, "'" + name + "%'");
                preparedStatement.setString(2, brand);
            } else if (!model.isEmpty() && brand.isEmpty()) {
                query = "select id from " + DB_BRAND_TABLE + " b join " + DB_MODEL_TABLE + " m on (b.id=m.brand_id) where b.name LIKE ? and m.name=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, "'" + name + "%'");
                preparedStatement.setString(2, model);
            } else if (!brand.isEmpty() && !model.isEmpty()) {
                query = "select id from " + DB_BRAND_TABLE + " b join " + DB_MODEL_TABLE + " m on (b.id=m.brand_id) where b.name LIKE ? and b.name=? and m.name=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, "'" + name + "%'");
                preparedStatement.setString(2, brand);
                preparedStatement.setString(2, model);
            } else {
                query = "select id from " + DB_BRAND_TABLE + " b join " + DB_MODEL_TABLE + " m on (b.id=m.brand_id) where name LIKE ?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, "'" + name + "%'");
            }
            resultSet = preparedStatement.executeQuery();
            List<Integer> ids = new ArrayList<Integer>();
            while (resultSet.next()) {
                ids.add(resultSet.getInt(1));
            }
            return ids;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public List<Integer> getAllModelIds(String name, String brand, String model) {
        try {
            connection = getConnection();
            String query;
            if (!brand.isEmpty() && model.isEmpty()) {
                query = "select id from " + DB_MODEL_TABLE + " m join " + DB_BRAND_TABLE + " b on (b.id=m.brand_id) where m.name LIKE ? and b.name=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, "'" + name + "%'");
                preparedStatement.setString(2, brand);
            } else if (!model.isEmpty() && brand.isEmpty()) {
                query = "select id from " + DB_MODEL_TABLE + " b join " + DB_BRAND_TABLE + " m on (b.id=m.brand_id) where m.name LIKE ? and m.name=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, "'" + name + "%'");
                preparedStatement.setString(2, model);
            } else if (!brand.isEmpty() && !model.isEmpty()) {
                query = "select id from " + DB_MODEL_TABLE + " b join " + DB_BRAND_TABLE + " m on (b.id=m.brand_id) where m.name LIKE ? and b.name=? and m.name=?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, "'" + name + "%'");
                preparedStatement.setString(2, brand);
                preparedStatement.setString(2, model);
            } else {
                query = "select id from " + DB_MODEL_TABLE + " b join " + DB_BRAND_TABLE + " m on (b.id=m.brand_id) where m.name LIKE ?";
                preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, "'" + name + "%'");
            }
            resultSet = preparedStatement.executeQuery();
            List<Integer> ids = new ArrayList<Integer>();
            while (resultSet.next()) {
                ids.add(resultSet.getInt(1));
            }
            return ids;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public List<CarModel> getAllModelsByBrandId(int brandId) {
        try {
            String query = "select * from " + DB_MODEL_TABLE + " where brand_id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, brandId);
            resultSet = preparedStatement.executeQuery();
            List<CarModel> models = new ArrayList<CarModel>();
            while (resultSet.next()) {
                CarModel model = new CarModel();
                model.setId(resultSet.getInt(1));
                model.setName(resultSet.getString(2));
                models.add(model);
            }
            return models;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

//    String query = "select c.id, c.status, c.imageUrl, b.name, m.name, c.description from " + CarDAO.DB_CAR_TABLE + " c join " + CarDAO.DB_MODEL_TABLE + " m on(c.model_id=m.id) join " + CarDAO.DB_BRAND_TABLE + " b on(m.brand_id=b.id) where c.status='"+CarStatus.valueOf(status)+"' and c.price between " + min + " and " + max;
    public List<Car> getSearchResult(String query) {
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            List<Car> cars = new ArrayList<Car>();
            while (resultSet.next()) {
                Car car = new Car();
                car.setId(resultSet.getInt(1));
                car.setStatus(CarStatus.valueOf(resultSet.getString(2)));
                car.setImageUrl(resultSet.getString(3));
                CarBrand carBrand = new CarBrand();
                carBrand.setName(resultSet.getString(4));
                CarModel carModel = new CarModel();
                carModel.setBrand(carBrand);
                carModel.setName(resultSet.getString(5));
                car.setModel(carModel);
                car.setDescription(resultSet.getString(6));
                cars.add(car);
            }
            return cars;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public List<CarModel> getAllModelsByBrandName(String name) {
        try {
            String query = "select m.id, m.name from " + DB_MODEL_TABLE + " m join " + DB_BRAND_TABLE + " b on (b.id=m.brand_id) where b.name=?";
            System.out.println(query);
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            List<CarModel> models = new ArrayList<CarModel>();
            while (resultSet.next()) {
                CarModel model = new CarModel();
                model.setId(resultSet.getInt(1));
                model.setName(resultSet.getString(2));
                models.add(model);
            }
            return models;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public CarModel getModelById(int id) throws SQLException {
        String query = "select * from " + DB_MODEL_TABLE + " where id=?";
//        connection = getConnection();
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            CarModel model = new CarModel();
            model.setId(resultSet.getInt(1));
            model.setName(resultSet.getString(2));
            model.setBrandId(resultSet.getInt(3));
            model.setBrand(getBrandById(model.getBrandId()));
            return model;
        }
        return null;
    }

    public CarBrand getBrandById(int id) throws SQLException {
        String query = "select * from " + DB_BRAND_TABLE + " where id=?";
        connection = getConnection();
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            CarBrand brand = new CarBrand();
            brand.setId(resultSet.getInt(1));
            brand.setName(resultSet.getString(2));
            return brand;
        }
        return null;
    }

    public List<Car> search(CarSearch carSearch, boolean withRelation, HttpServletRequest request) {
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(carSearch.getStrictSearch());
            resultSet = preparedStatement.executeQuery();
            List<Car> cars = new ArrayList<Car>();
            while (resultSet.next()) {
                Car car = getCar(request);
                if (withRelation) {
                    car.setModel(getModelById(car.getId()));
                }
                cars.add(car);
            }
            return cars;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public ArrayList<String> selectImagesByCarId(String uuid) throws SQLException {
        String query = "select url from " + DB_IMAGE_TABLE + " where car_uuid=?";
        System.out.println(query);
        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, uuid);
        resultSet = preparedStatement.executeQuery();
        ArrayList<String> urls = new ArrayList<String>();
        while (resultSet.next()) {
            urls.add(resultSet.getString("url"));
        }
        return urls;
    }

//    @Override
    public Car selectById(int id, boolean withRelation, HttpServletRequest request, CarStatus carStatus) {
        try {
            String query;
            if (carStatus.equals(CarStatus.ANY)) {
                query = "select * from " + DB_CAR_TABLE + " where id=? ";
            } else {
                query = "select * from " + DB_CAR_TABLE + " where id=? and status!='NOT_AVAILABLE'";
            }
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            Car car = null;
            while (resultSet.next()) {
                car = getCar(request);
                if (withRelation) {
                    car.setModel(getModelById(car.getModelId()));
                }
            }
            ArrayList<String> urls = selectImagesByCarId(car.getUuid());
            urls.add(car.getImageUrl());
            car.setImageUrls(urls);
            car.setUser(UserDAO.getInstance().selectById(car.getUserId()));
            return car;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public Car selectByUUID(String uuid, boolean withRelation, HttpServletRequest request, CarStatus carStatus) {
        try {
            String query;
            if (carStatus.equals(CarStatus.ANY)) {
                query = "select * from " + DB_CAR_TABLE + " where uuid=? ";
            } else {
                query = "select * from " + DB_CAR_TABLE + " where uuid=? and status!='NOT_AVAILABLE'";
            }
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, uuid);
            resultSet = preparedStatement.executeQuery();
            Car car = null;
            while (resultSet.next()) {
                car = getCar(request);
                if (withRelation) {
                    car.setModel(getModelById(car.getModelId()));
                }
            }
            ArrayList<String> urls = selectImagesByCarId(car.getUuid());
            urls.add(car.getImageUrl());
            car.setImageUrls(urls);
            car.setUser(UserDAO.getInstance().selectById(car.getUserId()));
            return car;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public int selectUserIdByCarId(int carId) {
        try {

            String query = "select user_id from " + DB_CAR_TABLE + " where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, carId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return -1;
    }

    public List<Car> selectByUserId(long userId, int limit, boolean withRelation, HttpServletRequest request, CarStatus carStatus) {
        try {
            String query;

            if (limit > 0) {
                if (carStatus.equals(CarStatus.ANY)) {
                    query = "select * from " + DB_CAR_TABLE + " where user_id=? limit " + limit;
                } else {
                    query = "select * from " + DB_CAR_TABLE + " where user_id=? and status!='NOT_AVAILABLE' limit " + limit;
                }
            } else if (carStatus.equals(CarStatus.ANY)) {
                query = "select * from " + DB_CAR_TABLE + " where user_id=?";
            } else {
                query = "select * from " + DB_CAR_TABLE + " where user_id=? and status!='NOT_AVAILABLE'";
            }
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();
            List<Car> cars = new ArrayList<Car>();
            while (resultSet.next()) {
                Car car = getCar(request);
                if (withRelation) {
                    car.setModel(getModelById(car.getId()));
                }
                cars.add(car);
            }
            return cars;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeAll();
        }
    }

    public Car selectCarsByStatus(CarStatus status, boolean withRelation, HttpServletRequest request) {
        try {
            String query = "select * from " + DB_CAR_TABLE + " where status=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, status.toString());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Car car = getCar(request);
                if (withRelation) {
                    car.setModel(getModelById(car.getId()));
                }
                return car;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
        return null;
    }

    public long selectCarCountByStatus(CarStatus status) {
        try {
            String query = "select count(id) as c from " + DB_CAR_TABLE + " where status=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, status.toString());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getLong("c");
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            closeAll();
        }
    }

    public JSONObject advancedSearch(AbstractSearchEngine data) {
        JSONObject result = new JSONObject();
        JSONArray array = new JSONArray();
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(data.getQuery());
            resultSet = preparedStatement.executeQuery();
            JSONArray ja = new JSONArray();
            while (resultSet.next()) {
                ja.add(resultSet.getString(1));
                ja.add(resultSet.getString(2));
                ja.add(resultSet.getString(3));
                ja.add(resultSet.getString(4));
                ja.add(resultSet.getString(5));
                array.add(ja);
            }
            result.put("iTotalRecords", data.getTotalDataCount());
            result.put("iTotalDisplayRecords", ja.size());
            result.put("aaData", array);
            return result;
        } catch (Exception e) {
            return null;
        } finally {
            closeAll();
        }
    }

    private Car getCar(HttpServletRequest quest) throws SQLException {
        Car car = new Car();
        car.setId(resultSet.getInt(C[0]));
        car.setModelId(resultSet.getInt(C[1]));
        car.setColor(resultSet.getString(C[2]));
        car.setDescription(resultSet.getString(C[3]));
        car.setImageUrl(resultSet.getString(C[4]));
        car.setHorsePower(resultSet.getInt(C[5]));
        car.setStatus(CarStatus.valueOf(resultSet.getString(C[6])));
        car.setYear(resultSet.getInt(C[7]));
        car.setEngineVolume(resultSet.getInt(C[8]));

        car.setFuelType(FuelType.valueOf(resultSet.getString(C[9])));
        car.setTransmissionType(TransmissionType.valueOf(resultSet.getString(C[10])));
        car.setDifferentialType(DifferentialType.valueOf(resultSet.getString(C[11])));
        car.setUserId(resultSet.getInt(C[12]));
        car.setPrice(resultSet.getDouble(C[13]));
        car.setRentType(RentType.valueOf(resultSet.getString(C[14])));
        car.setCurrency(resultSet.getString(C[15]));
        car.setUuid(resultSet.getString(C[16]));
        return car;
    }

    private void closeAll() {
        try {
            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException ex) {
            Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
