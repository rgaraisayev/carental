/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.dao;

import static az.edu.ada.carental.dao.CarDAO.C;
import az.edu.ada.carental.dao.abstractDatabase.DatabaseConnector;
import az.edu.ada.carental.dao.abstractDatabase.IDatabaseCRUD;
import az.edu.ada.carental.entities.Car;
import az.edu.ada.carental.entities.Request;
import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.RequestStatus;
import az.edu.ada.carental.enums.UserStatus;
import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class RequestDAO extends DatabaseConnector implements IDatabaseCRUD<Request> {

    protected ResultSet resultSet;
    protected PreparedStatement preparedStatement;
    protected Connection connection;

    public static final String DB_REQUEST_TABLE = "REQUESTS";
    /* 9 rows*/
    public static final String C[] = {"id", "user_id", "car_uuid", "status", "date"};

    private static RequestDAO requestDAO;

    public static RequestDAO getInstance() {
        return requestDAO != null ? requestDAO : (requestDAO = new RequestDAO());
    }

    @Override
    public boolean insert(Request data) {
        try {
            String query = "insert into " + DB_REQUEST_TABLE + "(" + C[1] + "," + C[2] + "," + C[3] + "," + C[4] + ") "
                    + "values(?,?,?,?)";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, data.getUser().getId());
            preparedStatement.setString(2, data.getCar().getUuid());
            preparedStatement.setString(3, data.getStatus().toString());
            preparedStatement.setDate(4, data.getDate());
            preparedStatement.execute();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public boolean delete(Request data) {
        try {
            String query = "delete from " + DB_REQUEST_TABLE + " where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, data.getId());
            preparedStatement.execute();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public boolean update(Request data) {
        try {
            String query = "update " + DB_REQUEST_TABLE + " set " + C[1] + "=?," + C[2] + "=?," + C[3] + "=?," + C[4] + "=? where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(5, data.getId());
            preparedStatement.setLong(1, data.getUser().getId());
            preparedStatement.setString(2, data.getCar().getUuid());
            preparedStatement.setString(3, data.getStatus().toString());
            preparedStatement.setDate(4, data.getDate());
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean updateStatusById(long id, RequestStatus rs) {
        try {
            String query = "update " + DB_REQUEST_TABLE + " set " + C[3] + "=? where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, rs.toString());
            preparedStatement.setLong(2, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<Request> selectAll() {
        try {
            String query = "select * from " + DB_REQUEST_TABLE;
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            List<Request> requests = new ArrayList<Request>();
            Request request;
            while (resultSet.next()) {
                request = new Request();
                request.setId(resultSet.getLong(C[0]));
                User user = new User();
                user.setId(resultSet.getInt(C[1]));
                Car car = new Car();
                car.setUuid(resultSet.getString(C[2]));
                request.setStatus(RequestStatus.valueOf(resultSet.getString(C[3])));
                request.setDate(resultSet.getDate(C[4]));
                request.setUser(user);
                request.setCar(car);
                requests.add(request);
            }
            return requests;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    public List<Request> selectAllWithRelations(HttpServletRequest re) {
        try {
//            String query = "select u.id, u.name, u.surname, u.username, r.id, r.date, r.status, c.id, c.year, c.status, c.horsepower, c.model, c.brand, c.color, c.description, c.imageurl  from " + DB_REQUEST_TABLE + " r left join " + CarDAO.DB_CAR_TABLE + " c on r.car_id=c.id join " + UserDAO.DB_USER_TABLE + " u on r.user_id = u.id and c.user_id=u.id order by r.id";
            String query = "Select * from " + DB_REQUEST_TABLE;
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            List<Request> requests = new ArrayList<Request>();
            Request request;
            while (resultSet.next()) {
                request = new Request();
                User user = new User();
                Car car = new Car();
                car = CarDAO.getInstance().selectById(resultSet.getInt("car_id"), true, re, CarStatus.ANY);
                user = UserDAO.getInstance().selectById(resultSet.getInt("user_id"));
                request.setId(resultSet.getLong(1));
                request.setDate(resultSet.getDate(5));
                request.setStatus(RequestStatus.valueOf(resultSet.getString(4)));
                request.setUser(user);
                request.setCar(car);
                requests.add(request);
            }
            return requests;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    public Request selectById(Request data, HttpServletRequest re) {
        try {
//            String query = "select u.id, u.name, u.surname, u.username, r.id, r.date, r.status, c.id, c.year, c.status, c.horsepower, c.model, c.brand, c.color, c.description, c.imageurl  from " + DB_REQUEST_TABLE + " r left join " + CarDAO.DB_CAR_TABLE + " c on r.car_id=c.id join " + UserDAO.DB_USER_TABLE + " u  on r.user_id = u.id where r.id=?";
            String query = "Select * from " + DB_REQUEST_TABLE + " where id=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, data.getId());
            resultSet = preparedStatement.executeQuery();
            Request request = null;
            if (resultSet.next()) {
                request = new Request();
                User user = new User();
                Car car = new Car();
                car = CarDAO.getInstance().selectById(resultSet.getInt("car_id"), true, re, CarStatus.ANY);
                user = UserDAO.getInstance().selectById(resultSet.getInt("user_id"));
                request.setId(resultSet.getLong(1));
                request.setDate(resultSet.getDate(5));
                request.setStatus(RequestStatus.valueOf(resultSet.getString(4)));
                request.setUser(user);
                request.setCar(car);
            }
            return request;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }
    //select u.id, u.name, u.surname, u.username, r.id, r.date, r.status, c.id, c.year, c.status, c.horsepower, c.model, c.brand, c.color, c.description, c.imageurl  from REQUESTS r left join CARS c on r.car_id=c.id  left join USERS u r.user_id = u.id and r.status=? order by r.id desc

    public List<Request> selectRequestsByStatus(RequestStatus status, HttpServletRequest re) {
        try {
//            String query = "select u.id, u.name, u.surname, u.username, r.id, r.date, r.status, c.id, c.year, c.status, c.horsepower, c.model, c.brand, c.color, c.description, c.imageurl  from " + DB_REQUEST_TABLE + " r left join " + CarDAO.DB_CAR_TABLE + " c on r.car_id=c.id join " + UserDAO.DB_USER_TABLE + " u on r.user_id = u.id where r.status=? order by r.id desc";
            String query = "Select * from " + DB_REQUEST_TABLE + " where status=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, status.toString());
            resultSet = preparedStatement.executeQuery();
            List<Request> requests = new ArrayList();
            Request request = null;
            while (resultSet.next()) {
                request = new Request();
                User user = new User();
                Car car = new Car();
                car = CarDAO.getInstance().selectByUUID(resultSet.getString("car_uuid"), true, re, CarStatus.ANY);
                user = UserDAO.getInstance().selectById(resultSet.getInt("user_id"));
                request.setId(resultSet.getLong(1));
                request.setDate(resultSet.getDate(5));
                request.setStatus(RequestStatus.valueOf(resultSet.getString(4)));
                request.setUser(user);
                request.setCar(car);
                requests.add(request);
            }
            return requests;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    public long selectRequestCountByStatus(RequestStatus status) {
        try {
            String query = "select count(id) as c from " + DB_REQUEST_TABLE + " where status=?";
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, status.toString());            
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getLong("c");
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    public long selectRequestCount() {
        try {
            String query = "select count(id) as c from " + DB_REQUEST_TABLE;
            connection = getConnection();
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getLong("c");
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    public JSONObject advancedSearch(AbstractSearchEngine data) {
        JSONObject result = new JSONObject();
        JSONArray array = new JSONArray();
        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(data.getQuery());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                System.out.println(resultSet.getString(1));
                JSONObject ja = new JSONObject();
                ja.put(C[0], resultSet.getString(1));
                ja.put(C[1], resultSet.getString(2));
                ja.put(C[2], resultSet.getString(3));
                ja.put(C[3], resultSet.getString(4));
                ja.put(C[4], resultSet.getString(5));
                array.add(ja);
            }

            result.put("totalCount", selectRequestCount());
            result.put("count", array.size());
            result.put("requests", array);
            System.out.println(result.toJSONString());
            return result;
        } catch (Exception e) {
            return null;
        } finally {
            try {
                connection.close();
                preparedStatement.close();
                resultSet.close();
            } catch (SQLException ex) {
            }
        }
    }

}
