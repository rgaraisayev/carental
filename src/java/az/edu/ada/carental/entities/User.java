/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.entities;

import az.edu.ada.carental.enums.UserType;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class User implements HttpSessionBindingListener {

    private int id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String phone;
    private String profileImageUrl;
    private UserType type;
    private String uuid;
    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        HttpSession session = event.getSession();
        Long onlineUsers = (Long) session.getServletContext().getAttribute("onlineUsers");
        if (onlineUsers != null) {
            onlineUsers++;
            session.getServletContext().setAttribute("onlineUsers", onlineUsers);
        } else {
            session.getServletContext().setAttribute("onlineUsers", (long) 1);
        }
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        HttpSession session = event.getSession();
        Long onlineUsers = (Long) session.getServletContext().getAttribute("onlineUsers");
        if (onlineUsers != null) {
            onlineUsers--;
            session.getServletContext().setAttribute("onlineUsers", onlineUsers);
        } else {
            session.getServletContext().setAttribute("onlineUsers", (long) 0);
        }
    }
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
