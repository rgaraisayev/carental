/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.edu.ada.carental.entities;

/**
 *
 * @author Ramazan Garaisayev
 * @email  rgaraisayev2018@gmail.com
 *
 */
public class Color {
    public static final String BLACK = "BLACK";
    public static final String WHITE = "WHITE";
    public static final String PURPLE = "PURPLE";
    public static final String RED = "RED";
    public static final String YELLOW = "YELLOW";
    public static final String PINK = "PINK";
    public static final String GREEN = "GREEN";
    public static final String BLUE = "BLUE";
    public static final String CUSTOM = "";
}
