/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.edu.ada.carental.entities;

import java.sql.Date;

/**
 *
 * @author Ramazan Garaisayev
 * @email  rgaraisayev2018@gmail.com
 *
 */
public class BlackList {
    private int id;
    private int user_id;
    private boolean isPermanent;
    private Date startDate;
    private Date endDate;
    private boolean IPBlockOnly;
    
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public boolean isIsPermanent() {
        return isPermanent;
    }

    public void setIsPermanent(boolean isPermanent) {
        this.isPermanent = isPermanent;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isIPBlockOnly() {
        return IPBlockOnly;
    }

    public void setIPBlockOnly(boolean IPBlockOnly) {
        this.IPBlockOnly = IPBlockOnly;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
