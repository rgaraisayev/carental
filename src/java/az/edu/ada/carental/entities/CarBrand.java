/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.edu.ada.carental.entities;

/**
 *
 * @author Ramazan Garaisayev
 * @email  rgaraisayev2018@gmail.com
 *
 */
public class CarBrand {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
