/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.edu.ada.carental.entities;

import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.car.FuelType;
import az.edu.ada.carental.enums.RentType;
import az.edu.ada.carental.enums.car.DifferentialType;
import az.edu.ada.carental.enums.car.TransmissionType;
import java.util.ArrayList;

/**
 *
 * @author Ramazan Garaisayev
 * @email  rgaraisayev2018@gmail.com
 *
 */
public class Car {
    private String uuid;
    private int id;
    private int modelId;
    private String color;
    private String description;
    private int horsePower;
    private String imageUrl;
    private CarStatus status;
    private int year;
    private int engineVolume;
    private FuelType fuelType;
    private TransmissionType transmissionType;
    private DifferentialType differentialType;  
    private int userId;
    private double price;
    private RentType rentType;
    private String currency;
    private User user;
    private ArrayList<String> imageUrls;
    private CarModel model;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    public ArrayList<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(ArrayList<String> imageUrls) {
        this.imageUrls = imageUrls;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id =  id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public CarStatus getStatus() {
        return status;
    }

    public void setStatus(CarStatus status) {
        this.status = status;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public RentType getRentType() {
        return rentType;
    }

    public void setRentType(RentType rentType) {
        this.rentType = rentType;
    }
 

    /**
     * @return the modelId
     */
    public int getModelId() {
        return modelId;
    }

    /**
     * @param modelId the modelId to set
     */
    public void setModelId(int modelId) {
        this.modelId = modelId;
    }
 
    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getEngineVolume() {
        return engineVolume;
    }

    public void setEngineVolume(int engineVolume) {
        this.engineVolume = engineVolume;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public TransmissionType getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(TransmissionType transmissionType) {
        this.transmissionType = transmissionType;
    }

    public DifferentialType getDifferentialType() {
        return differentialType;
    }

    public void setDifferentialType(DifferentialType differentialType) {
        this.differentialType = differentialType;
    }

    public CarModel getModel() {
        return model;
    }

    public void setModel(CarModel model) {
        this.model = model;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
}
