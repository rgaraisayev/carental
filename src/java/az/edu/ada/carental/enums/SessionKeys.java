/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.edu.ada.carental.enums;

/**
 *
 * @author Ramazan Garaisayev
 * @email  rgaraisayev2018@gmail.com
 *
 */
public class SessionKeys {
    public static final String IS_LOGGED = "isLogged";
    public static final String LOGGED_USER = "loggedUser";

}
