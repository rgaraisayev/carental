/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.enums;

/**
 *
 * @author ramco
 */
public enum CarStatus {
    NOT_AVAILABLE,
    AVAILABLE,
    AT_RENT,
    ACCEPTED,
    REJECTED,
    PENDING,
    ANY,
    NOT_ANY;
}
