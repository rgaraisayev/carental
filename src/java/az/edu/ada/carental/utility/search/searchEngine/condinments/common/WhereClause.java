/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.utility.search.searchEngine.condinments.common;

import az.edu.ada.carental.enums.sql.SQLOperations;
import az.edu.ada.carental.enums.sql.SQLWhereTypes;
import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import az.edu.ada.carental.utility.search.searchEngine.condinments.CondinmentDecorator;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class WhereClause extends CondinmentDecorator {

    private String[] columns;
    private String[] values;
    private SQLWhereTypes[] whereTypes;
    private SQLOperations[] operations;
    private final String LIKE = "like";

    public WhereClause(AbstractSearchEngine searchEngine, String[] columns, SQLWhereTypes[] whereTypes, String[] values, SQLOperations[] operations) {
        super(searchEngine);
        this.columns = columns;
        this.values = values;
        this.whereTypes = whereTypes;
        this.operations = operations;
        if (columns.length != whereTypes.length || whereTypes.length != values.length) {
            throw new UnsupportedOperationException("Not same parameters");

        } else {
            constructQuery("");
        }

    }

    @Override
    public String getQuery() {
        return searchQuery;

    }

    @Override
    public void constructQuery(String whereTypet) {
        String whereClause = "";
        searchQuery = " where "; // select * from table where a=1;
        for (int i = 0; i < columns.length; i++) {
            if (whereTypes[i].equals(SQLWhereTypes.EQUAL)) {
                whereClause += columns[i] + "=" + values[i];
            } else if (whereTypes[i].equals(SQLWhereTypes.LIKE)) {
                whereClause += columns[i] + " like '%" + values[i] + "%'";
            } else if (whereTypes[i].equals(SQLWhereTypes.BETWEEN)) {
                whereClause += columns[i] + " between " + values[i];
            }
            if (i < columns.length - 1) {
                if (operations[i].equals(SQLOperations.AND)) {
                    whereClause += " and ";
                } else if (operations[i].equals(SQLOperations.OR)) {
                    whereClause += " or ";
                }
            }
        }
        String query = searchEngine.getQuery();
        if (query.contains("order")) {
            int index = query.indexOf("order");
            String qPartOne = query.substring(0, index);
            String qPartTwo = query.substring(index, query.length());
            searchQuery = qPartOne + " where " + whereClause + qPartTwo;
        } else {
            searchQuery = query + " where " + searchQuery;

        }
    }

}
