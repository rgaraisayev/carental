/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.utility.search.domen;

import az.edu.ada.carental.dao.CarDAO;
import az.edu.ada.carental.entities.CarModel;
import az.edu.ada.carental.entities.User;
import az.edu.ada.carental.enums.CarStatus;
import az.edu.ada.carental.enums.RentType;
import az.edu.ada.carental.enums.car.DifferentialType;
import az.edu.ada.carental.enums.car.FuelType;
import az.edu.ada.carental.enums.car.TransmissionType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class CarSearch {

    private String color;
    private int horsePower;
    private CarStatus status;
    private int year;
    private int engineVolume;
    private FuelType fuelType;
    private TransmissionType transmissionType;
    private DifferentialType differentialType;
    private int userId;
    private double price;
    private RentType rentType;
    private String currency;
    private CarModel carModel;
    private String model;
    private int limit;
    private int offset;
    private PreparedStatement preparedStatement;
    private String strictQuery;

    public void setColor(String color) {
        this.color = color;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public void setStatus(CarStatus status) {
        this.status = status;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setEngineVolume(int engineVolume) {
        this.engineVolume = engineVolume;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public void setTransmissionType(TransmissionType transmissionType) {
        this.transmissionType = transmissionType;
    }

    public void setDifferentialType(DifferentialType differentialType) {
        this.differentialType = differentialType;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setRentType(RentType rentType) {
        this.rentType = rentType;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

//    public static final String C[] = {"id", "model_id", "color", "description", "imageurl", "horse_power", "status",
//        "year", "engine_volume", "fuel_type", "transmission_type", "differential_type",
//        "user_id", "price", "rent_type", "currency"};
    public String getStrictSearch() throws SQLException {
        String query = "select * from " + CarDAO.DB_CAR_TABLE + " ";
        String where = "where ";
        if (color != null && !color.isEmpty() && !color.contains("'") && !color.contains("\"") && !color.contains(";")) {
            where += " " + CarDAO.C[2] + "='" + color + "'";
        }
        if (horsePower > 0) {
            where += " " + CarDAO.C[5] + "= " + horsePower;
        }

        if (status != null) {
            where += " " + CarDAO.C[6] + "= " + status.toString();
        }
        if (year > 0) {
            where += " " + CarDAO.C[7] + "= " + year;
        }

        if (engineVolume > 0) {
            where += " " + CarDAO.C[8] + "= " + engineVolume;
        }

        if (fuelType != null) {
            where += " " + CarDAO.C[9] + "= " + fuelType.toString();
        }

        if (transmissionType != null) {
            where += " " + CarDAO.C[10] + "= " + transmissionType.toString();
        }

        if (differentialType != null) {
            where += " " + CarDAO.C[11] + "= " + differentialType.toString();
        }

        if (userId > 0) {
            where += " " + CarDAO.C[12] + "=? ";
            preparedStatement.setInt(9, userId);
        }

        if (price > 0) {
            where += " " + CarDAO.C[13] + "= " + price;
        }

        if (rentType != null) {
            where += " " + CarDAO.C[14] + "= " + rentType.toString();
        }

        if (currency != null && !currency.isEmpty() && !currency.contains("'") && !currency.contains("\"") && !currency.contains(";")) {
            where += " " + CarDAO.C[15] + "= '" + currency + "'";
        }

        if (limit >= 0) {
            where += " limit  " + limit;
        }

        if (offset > 0) {
            where += " " + offset;
        }
        strictQuery = query + where;
        return strictQuery;
    }

    public PreparedStatement getLikeSearch(PreparedStatement preparedStatement) {

        return preparedStatement;
    }

    public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    public String getStrictQuery() {
        return strictQuery;
    }
}
