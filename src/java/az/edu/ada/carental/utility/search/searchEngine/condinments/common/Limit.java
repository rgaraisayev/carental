/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.utility.search.searchEngine.condinments.common;

import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import az.edu.ada.carental.utility.search.searchEngine.condinments.CondinmentDecorator;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class Limit extends CondinmentDecorator {

    private int end;

    public Limit(AbstractSearchEngine searchEngine, int begin, int end) {
        super(searchEngine);
        this.end = end;
        constructQuery(String.valueOf(begin));
    }

    @Override
    public void constructQuery(String limit) {
        searchQuery = searchEngine.getQuery() + " limit " + limit + ", " + end;
    }

    @Override
    public String getQuery() {
        return searchQuery;
    }

}
