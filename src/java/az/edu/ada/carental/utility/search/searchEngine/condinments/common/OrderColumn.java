/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.utility.search.searchEngine.condinments.common;

import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import az.edu.ada.carental.utility.search.searchEngine.condinments.CondinmentDecorator;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class OrderColumn extends CondinmentDecorator {

    public OrderColumn(AbstractSearchEngine searchEngine, String column) {
        super(searchEngine);
        constructQuery(column);
    }

    @Override
    public void constructQuery(String column) {
        if (!searchEngine.getQuery().contains("order by")) {
            searchQuery = searchEngine.getQuery() + " order by " + column;
        } else {
            searchQuery = searchEngine.getQuery() + " " + column;
        }
    }

    @Override
    public String getQuery() {
        return searchQuery;
    }
}
