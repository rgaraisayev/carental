/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.utility.search.searchEngine;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class FullSearch extends AbstractSearchEngine {

    private String tempSearchQuery;

    public FullSearch(String table, String[] clmns) {
        this.clmns = clmns;
        this.table = table;
        initQuery(table, clmns);
        tempSearchQuery = getQuery();
    }

    @Override
    protected void initQuery(String table, String[] clmn) {
        this.clmns = clmn;
        this.table = table;
        searchQuery = "select ";
        for (String c : clmn) {
            searchQuery += c + ", ";
        }
        searchQuery = searchQuery.substring(0, searchQuery.length() - 2);
        searchQuery += " from " + table;
    }

    @Override
    public String getQuery() {
        return searchQuery;
    }

    @Override
    public void constructQuery(String value) {
        if (clmns != null && clmns.length > 0) {
            String columns = tempSearchQuery + " where ";
            for (String c : clmns) {
                columns += c + " like '%" + value + "%' or ";
            }
            searchQuery = columns;
            searchQuery = searchQuery.substring(0, searchQuery.length() - 3);
        }
    }

}
