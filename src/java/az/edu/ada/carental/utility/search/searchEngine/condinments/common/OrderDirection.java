/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.utility.search.searchEngine.condinments.common;

import az.edu.ada.carental.enums.sql.SQLOrderType;
import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import az.edu.ada.carental.utility.search.searchEngine.condinments.CondinmentDecorator;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public class OrderDirection extends CondinmentDecorator {

    public final String ASC = "asc";
    public final String DESC = "desc";

    public OrderDirection(AbstractSearchEngine searchEngine, SQLOrderType direction) {
        super(searchEngine);
        if (direction.equals(SQLOrderType.ASC)) {
            constructQuery(ASC);
        } else if (direction.equals(SQLOrderType.DESC)) {
            constructQuery(DESC);
        }
    }

    @Override
    public void constructQuery(String direct) {
        if (!searchEngine.getQuery().contains("order by")) {
            searchQuery = searchEngine.getQuery() + " order by " + direct;
        } else {
            searchQuery = searchEngine.getQuery() + " " + direct;
        }
    }

    @Override
    public String getQuery() {
        return searchQuery;
    }

}
