/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package az.edu.ada.carental.utility.search.searchEngine;

import az.edu.ada.carental.dao.abstractDatabase.DatabaseConnector;

/**
 *
 * @author Ramazan Garaisayev
 * @email rgaraisayev2018@gmail.com
 *
 */
public abstract class AbstractSearchEngine {

    public int START_PAGE_NO;
    public int VISIBLE_DATA_COUNT = 10;
    public long TOTAL_DATA_COUNT;
    public String searchQuery;
    public String table;
    public String[] clmns;
    public String singleColumn;

    protected void initQuery(String table, String[] clmn) {
        /// to be constructed
    }

    public abstract void constructQuery(String value);

    public abstract String getQuery();

    public long getTotalDataCount() {
        return TOTAL_DATA_COUNT;
    }

    public void setTotalDataCount(long count) {
        this.TOTAL_DATA_COUNT = count;

    }

    public String getColumn() {
        return singleColumn;
    }

    public void setColumn(String column) {
        this.singleColumn = singleColumn;
    }
    

}
