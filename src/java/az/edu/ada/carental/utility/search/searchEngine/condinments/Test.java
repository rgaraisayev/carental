/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.edu.ada.carental.utility.search.searchEngine.condinments;

import az.edu.ada.carental.enums.sql.SQLOperations;
import az.edu.ada.carental.enums.sql.SQLOrderType;
import az.edu.ada.carental.enums.sql.SQLWhereTypes;
import az.edu.ada.carental.utility.search.searchEngine.condinments.common.OrderDirection;
import az.edu.ada.carental.utility.search.searchEngine.condinments.common.OrderColumn;
import az.edu.ada.carental.utility.search.searchEngine.AbstractSearchEngine;
import az.edu.ada.carental.utility.search.searchEngine.FullSearch;
import az.edu.ada.carental.utility.search.searchEngine.condinments.common.Limit;
import az.edu.ada.carental.utility.search.searchEngine.condinments.common.WhereClause;

/**
 *
 * @author Ramazan Garaisayev
 * @email  rgaraisayev2018@gmail.com
 *
 */
public class Test {

    
    public static void main(String[] args) {
//        AbstractSearchEngine abstractSearchEngine = new FullSearch("users", new String[]{"id", "name"});
//        abstractSearchEngine.constructQuery("1");
//        AbstractSearchEngine abstractSearchEngine = new FullSearch("users", new String[]{"*"});
//        abstractSearchEngine = new OrderColumn(abstractSearchEngine, "id");
//        abstractSearchEngine = new OrderDirection(abstractSearchEngine, SQLOrderType.DESC);
//        abstractSearchEngine = new Limit(abstractSearchEngine, 4, 10);
//        abstractSearchEngine = new WhereClause(abstractSearchEngine, new String[]{"id", "name"}, new SQLWhereTypes[]{SQLWhereTypes.EQUAL, SQLWhereTypes.LIKE}, new String[]{"3", "ramaz"}, new SQLOperations[]{SQLOperations.AND, SQLOperations.OR});
//        
        AbstractSearchEngine abstractSearchEngine = new OrderColumn(new OrderDirection(new FullSearch("users", new String[]{"id", "name"}), SQLOrderType.DESC), "id");
        
        System.out.println(abstractSearchEngine.getQuery());
    }
}
